package pe.gob.osiptelandroid.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;

public class CounterService extends Service {

    private int tiempo;
    private Counter timer;

    private SessionManager mSessionManager;

    private long valor;
    int s;
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSessionManager = new SessionManager(getApplicationContext());
        timer = new Counter();
        timer.start();
    }


    @Override
    public void onDestroy() {
        timer.cancel();
        mSessionManager.setStatus(30000);
        super.onDestroy();
    }



    private class Counter extends CountDownTimer {

        Counter() {
            super(mSessionManager.getStatus(), (long) 1000);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            String seconds;
            s = (int) ((millisUntilFinished / 1000) % 60);

            seconds = String.valueOf(s);

            if (s < 10) {
                seconds = "0" + s;
            }

            mSessionManager.setStatus(s*1000);

            //Toast.makeText(Contador.this, seconds, Toast.LENGTH_SHORT).show();
            Intent i = new Intent("count");
            i.putExtra("count", seconds);
            sendBroadcast(i);

            if (seconds.equals("01")){
                //mPresenter.sendNoDisponible(mSessionManager.getUserEntity().getIdAsociado());
                stopService(new Intent(getApplicationContext(), CounterService.class));
                mSessionManager.setLoginTrasu(false);
                if(mSessionManager.getUserTrasu()!=null){
                    UserTrasuEntity userTrasuEntity = new UserTrasuEntity();
                    userTrasuEntity = mSessionManager.getUserTrasu();
                    userTrasuEntity.setNroDoc("");
                    mSessionManager.setUserTrasu(userTrasuEntity);
                }
                mSessionManager.setStatus(30000);
            }


        }

        @Override
        public void onFinish() {
            Log.e("FINISH", "onFinish");
            //textToSpeech.speak("Su refrigerio a concluido.", TextToSpeech.QUEUE_FLUSH, null);
        }

    }


}
