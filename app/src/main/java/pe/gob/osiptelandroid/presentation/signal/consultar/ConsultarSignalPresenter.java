package pe.gob.osiptelandroid.presentation.signal.consultar;

import android.content.Context;
import android.os.Build;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.CoberturaByOperadoraEntity;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class ConsultarSignalPresenter implements ConsultarSignalContract.Presenter {

    private ConsultarSignalContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public ConsultarSignalPresenter(ConsultarSignalContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getUbicacion() {

    }

    @Override
    public void getOperadores() {
        //  mView.showMessage(imei);
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<OperadoraEntity>> orders = listRequest.getOperadores("Bearer " + mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<OperadoraEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<OperadoraEntity>> call, Response<ArrayList<OperadoraEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);

                if (response.isSuccessful()) {
                    mView.getListOperadores(response.body());

                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener los operadores, intente nuevament por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<OperadoraEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getDepartamentos() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<DepartamentoEntity>> orders = listRequest.getDepartamentos("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<DepartamentoEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<DepartamentoEntity>> call, Response<ArrayList<DepartamentoEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if(response.body().size()!=0){
                        mView.getListDepartamentos(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener los departamentos, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DepartamentoEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getProvincia(final String codDepartamento) {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<ProvinciaEntity>> orders = listRequest.getProvincias("Bearer " + mSessionManager.getUserToken(), codDepartamento);
        orders.enqueue(new Callback<ArrayList<ProvinciaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ProvinciaEntity>> call, Response<ArrayList<ProvinciaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListProvincia(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener las provincias, intente nuevament por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ProvinciaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getDistrito(final String codDepartamento, final String codProvincia) {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<DistritoEntity>> orders = listRequest.getDistrito("Bearer " + mSessionManager.getUserToken(), codDepartamento, codProvincia);
        orders.enqueue(new Callback<ArrayList<DistritoEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<DistritoEntity>> call, Response<ArrayList<DistritoEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListDistrito(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener los distritos, intente nuevament por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DistritoEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getLocalidades(final String codDepartamento, final String codProvincia, final String codDistrito) {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<LocalidadEntity>> orders = listRequest.getLocalidades("Bearer " + mSessionManager.getUserToken(), codDepartamento, codProvincia, codDistrito);
        orders.enqueue(new Callback<ArrayList<LocalidadEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<LocalidadEntity>> call, Response<ArrayList<LocalidadEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListLocalidades(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener las localidades, intente nuevament por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }                }
            }

            @Override
            public void onFailure(Call<ArrayList<LocalidadEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void consultarCobertura(String departamento, String provincia, String distrito,
                                   String localidad, String idOperadora, String nombreUsuario, String numeroCel) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<CoberturaByOperadoraEntity>> orders = listRequest.getConsultaCobertura(
                "Bearer " + mSessionManager.getUserToken(), departamento, provincia,
                distrito, localidad, idOperadora, mSessionManager.getMac(),
                "Android", Build.BRAND, Build.MODEL, numeroCel, mSessionManager.getMac(), nombreUsuario);
        orders.enqueue(new Callback<ArrayList<CoberturaByOperadoraEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<CoberturaByOperadoraEntity>> call, Response<ArrayList<CoberturaByOperadoraEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListCobertura(response.body());
                    }
                } else {
                    mView.showErrorMessage("Error al obtener la cobertura");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CoberturaByOperadoraEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getOperadores();
                } else {
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
