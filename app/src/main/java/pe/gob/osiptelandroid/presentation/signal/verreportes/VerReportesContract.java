package pe.gob.osiptelandroid.presentation.signal.verreportes;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.VerReporteEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface VerReportesContract {
    interface View extends BaseView<Presenter> {

        void getListVerReportes(ArrayList<VerReporteEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getVerReportes(String departamento,
                               String provincia, String distrito, String localidad);




      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
