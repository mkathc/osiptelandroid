package pe.gob.osiptelandroid.presentation.encuestas;

import pe.gob.osiptelandroid.data.entities.PreguntaEntity;

/**
 * Created by katherine on 24/04/17.
 */

public interface EncuestaItem {

    void clickItem(PreguntaEntity preguntaEntity);
}
