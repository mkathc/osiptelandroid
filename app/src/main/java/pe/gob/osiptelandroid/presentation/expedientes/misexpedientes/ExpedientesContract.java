package pe.gob.osiptelandroid.presentation.expedientes.misexpedientes;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.ConsultaExpedienteEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface ExpedientesContract {
    interface View extends BaseView<Presenter> {

        void isLogin(UserTrasuEntity userTrasuEntity);
        void getListMisExpedientes(ArrayList<ConsultaExpedienteEntity> list);
        void getMyExpediente(String urlExpediente);

        void loadingPdf(boolean isLoading);

        void getListEstados(ArrayList<EstadoEntity> list);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getMisExpedientes(String nroDoc,
                               String estado, String fechaIni, String fechaFin,
                               String nombreUsuario, String numeroCel);

        void validarLogin(String correo, String contraseña, String ip, String nombreUsuario, String numeroCel);

        void getMyExpedientePDF(String nombreCompleto, String numeroDocumento, String idUsuario, String idExpediente);

        void getEstados();

        void getRefreshToken();




      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
