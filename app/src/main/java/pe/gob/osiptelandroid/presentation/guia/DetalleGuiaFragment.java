package pe.gob.osiptelandroid.presentation.guia;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.GuiaEntity;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.TouchImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetalleGuiaFragment extends BaseFragment {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.im_zoom)
    TouchImageView imZoom;
    @BindView(R.id.body)
    FrameLayout body;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    Unbinder unbinder;

    GuiaEntity guiaEntity;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;

    private String descripcion, nombreImagen;

    public DetalleGuiaFragment() {
        // Requires empty public constructor
    }

    public static DetalleGuiaFragment newInstance(Bundle bundle) {
        DetalleGuiaFragment detalleGuiaFragment = new DetalleGuiaFragment();
        detalleGuiaFragment.setArguments(bundle);
        return detalleGuiaFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.detalle_guia_informativa_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
      //  guiaEntity = (GuiaEntity) getArguments().getSerializable("guiaEntity");
        descripcion =  getArguments().getString("descripcion");
        nombreImagen = getArguments().getString("nombreImagen");

//        Log.e("ARCHIVO", guiaEntity.getNombreImagen());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });

        return root;
    }

    private void remove() {
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvTitle.setText(descripcion);
        progressBar1.setVisibility(View.VISIBLE);
        //   imZoom.setImageDrawable(guiaEntity.getImagen());
        if (nombreImagen != null) {
            Picasso.get()
                    .load(nombreImagen)
                    .into(imZoom, new Callback() {
                        @Override
                        public void onSuccess() {
                            if (progressBar1 != null) {
                                progressBar1.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
