package pe.gob.osiptelandroid.presentation.expedientes.registro_trasu;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.entities.TipoDocumentoEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyRegistrarTrasu;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.data.remote.request.PostRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class RegistroTrasuPresenter implements RegistroTrasuContract.Presenter {

    private RegistroTrasuContract.View mView;
    private Context context;
    private SessionManager mSessionManager;
    public RegistroTrasuPresenter(RegistroTrasuContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void sendRegistrotrasu(BodyRegistrarTrasu bodyRegistrarTrasu) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createTrasuService(PostRequest.class);
        Call<Void> orders = postRequest.sendRegitroTrasu("Bearer "+mSessionManager.getUserToken(),bodyRegistrarTrasu);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.showMessage("Su registro fue exitoso, por favor active su cuenta mediante el correo electrónico remitido");

                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getTipoDocumento() {
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ArrayList<TipoDocumentoEntity>> orders = listRequest.getTipoDocumento("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<TipoDocumentoEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<TipoDocumentoEntity>> call, Response<ArrayList<TipoDocumentoEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListTipoDocumentos(response.body());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TipoDocumentoEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getServicios() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ArrayList<ServiceEntity>> orders = listRequest.getListServicios("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<ServiceEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ServiceEntity>> call, Response<ArrayList<ServiceEntity>> response) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListTipoServicios(response.body());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ServiceEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }


    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getTipoDocumento();
                    getServicios();
                }else{
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
