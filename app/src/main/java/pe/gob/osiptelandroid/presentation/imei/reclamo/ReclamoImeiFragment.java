package pe.gob.osiptelandroid.presentation.imei.reclamo;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Select;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.MarcaEntity;
import pe.gob.osiptelandroid.data.entities.ModeloEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.entities.body.BodyReportarImei;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.ValidateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ReclamoImeiFragment extends BaseFragment implements ReclamoImeiContract.View, Validator.ValidationListener {

    @BindView(R.id.button_send)
    RelativeLayout buttonSend;
    Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @NotEmpty
    @Length(min = 15)
    @BindView(R.id.et_imei)
    EditText etImei;
    @Select
    @BindView(R.id.spinner_empresa)
    Spinner spinnerEmpresa;
    @NotEmpty
    @BindView(R.id.et_nombres)
    EditText etNombres;
    @NotEmpty
    @BindView(R.id.et_ap_paterno)
    EditText etApPaterno;
    @NotEmpty
    @BindView(R.id.et_ap_materno)
    EditText etApMaterno;
    @Select
    @BindView(R.id.spinner_marca)
    Spinner spinnerMarca;
    @BindView(R.id.spinner_modelo)
    Spinner spinnerModelo;
    @NotEmpty
    @Length(min = 9)
    @BindView(R.id.et_num_movil)
    EditText etNumMovil;
    @BindView(R.id.et_codigo_bloqueo)
    EditText etCodigoBloqueo;
    @BindView(R.id.tv_fecha_reporte)
    TextView tvFechaReporte;
    @BindView(R.id.tv_fecha_recuperacion)
    TextView tvFechaRecuperacion;
    @Length(max = 9)
    @BindView(R.id.et_num_contacto)
    EditText etNumContacto;
    @BindView(R.id.et_otra_marca)
    EditText etOtraMarca;
    @BindView(R.id.et_otro_modelo)
    EditText etOtroModelo;
    @BindView(R.id.tv_error_imei)
    TextView tvErrorImei;
    @BindView(R.id.ly_marca)
    LinearLayout lyMarca;
    @BindView(R.id.rl_spinner_marca)
    RelativeLayout rlSpinnerMarca;
    @NotEmpty
    @Email
    @BindView(R.id.et_correo_electronico)
    EditText etCorreoElectronico;
    @BindView(R.id.layout_container)
    ConstraintLayout layoutContainer;
    @NotEmpty
    @BindView(R.id.et_problema_detectado)
    TextView etProblemaDetectado;
    private ReclamoImeiContract.Presenter mPresenter;
    SingleDateAndTimePickerDialog.Builder singleBuilder;
    private int idMarca, idModelo, idEmpresOperadora, problema;
    private ProgressDialogCustom mProgressDialogCustom;
    SessionManager mSessionManager;
    Validator validator;
    private String imei;
    private boolean isOtraMarca = false;
    private boolean isOtroModelo = false;
    private String fechaReporte = "";
    private String fechaRecuperacion = "";
    private String descripcionMarca, descripcionOperadora, descripcionProblema;
    private String descripcionModelo = " ";
    private activateButtons mCallback;
    private ValidateUtils validateUtils;
    private ArrayList<ProblemaEntity> listProblema;
    private ProblemaEntity mProblema;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;

    public ReclamoImeiFragment() {
        // Requires empty public constructor
    }

    public static ReclamoImeiFragment newInstance(Bundle bundle) {
        ReclamoImeiFragment fragment = new ReclamoImeiFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public interface activateButtons {
        void sendActivateImeiButtons(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (activateButtons) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ReclamoImeiPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
        validator = new Validator(this);
        imei = getArguments().getString("imei");
        validateUtils = new ValidateUtils(getContext(), getActivity());
        mPresenterLog = new LogPresenter( getContext());
        bodyEntityLog = new BodyEntityLog();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.reportar_imei_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        //VALIDAR DATOS GUARDADOS EN SESSION MANAGEER
        if (mSessionManager.getUserEntity() != null) {
            setDataUser();
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });
        mPresenter.getEmpresas();
        layoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardView(layoutContainer);
            }
        });
        getActualDate();
        return root;
    }

    private void getActualDate() {
        final Calendar calendar = Calendar.getInstance();
        Date actualDate = calendar.getTime();
        Locale spanish = new Locale("es", "ES");
        SimpleDateFormat simpleDateFormatText = new SimpleDateFormat("EEE dd MMM hh:mm", spanish);
        tvFechaReporte.setText(simpleDateFormatText.format(actualDate));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator.setValidationListener(this);
        if (imei != null) {
            etImei.setText(imei);
        }
        updateValidationUi();
    }

    private void updateValidationUi() {
        //CLEAN VIEW WHE TYPE
        validateUtils.cleanUI(etNombres, null, false, 0);
        validateUtils.cleanUI(etApPaterno, null, false, 0);
        validateUtils.cleanUI(etApMaterno, null, false, 0);
        validateUtils.cleanUI(etCorreoElectronico, null, false, 0);
        validateUtils.cleanUI(etNumContacto, null, true, 9);
        validateUtils.cleanUI(etNumMovil, null, true, 9);
        validateUtils.cleanUI(etImei, tvErrorImei, true, 15);
    }

    public void hideKeyboardView(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void remove() {
        mCallback.sendActivateImeiButtons(true);
        mSessionManager.setProblemaNull();
        if(singleBuilder!=null){
            singleBuilder.dismiss();
        }
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    private void setDataUser() {
        etNumMovil.setText(mSessionManager.getUserEntity().getNroTelefono());
        etNombres.setText(mSessionManager.getUserEntity().getNombre());
        etApPaterno.setText(mSessionManager.getUserEntity().getApellidoPaterno());
        etApMaterno.setText(mSessionManager.getUserEntity().getApellidoMaterno());
        etCorreoElectronico.setText(mSessionManager.getUserEntity().getCorreoElectronico());

    }

    @Override
    public void getListMarca(final ArrayList<MarcaEntity> list) {

        ArrayList<String> listNombreMarca = new ArrayList<>();
        listNombreMarca.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listNombreMarca.add(list.get(i).getDescripcion());
        }
        listNombreMarca.add("Otros");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listNombreMarca);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMarca.setAdapter(dataAdapter);

        spinnerMarca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                descripcionMarca = spinnerMarca.getItemAtPosition(position).toString();

                if (descripcionMarca.equals("Otros")) {
                    etOtraMarca.setVisibility(View.VISIBLE);
                    isOtraMarca = true;
                } else {
                    if (!descripcionMarca.equals("Seleccionar")) {
                        spinnerMarca.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                        mPresenter.getModelo(getIdMarca(list, spinnerMarca.getItemAtPosition(position).toString()));
                    } else {
                        getListModelo(null);
                    }
                    etOtraMarca.setVisibility(View.GONE);
                }

                if (isOpenKeyboard()) {
                    hideKeyboard();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private int getIdMarca(ArrayList<MarcaEntity> list, String marca) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDescripcion().equals(marca)) {
                idMarca = list.get(i).getIdMarca();
            }
        }
        return idMarca;

    }

    @Override
    public void getListModelo(final ArrayList<ModeloEntity> list) {
        if (list != null) {
            ArrayList<String> listNombreModelo = new ArrayList<>();
            listNombreModelo.add("Seleccionar");
            for (int i = 0; i < list.size(); i++) {
                listNombreModelo.add(list.get(i).getDescripcion());
            }
            listNombreModelo.add("Otros");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, listNombreModelo);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerModelo.setAdapter(dataAdapter);

            spinnerModelo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    descripcionModelo = spinnerModelo.getItemAtPosition(position).toString();

                    if (descripcionModelo.equals("Otros")) {
                        etOtroModelo.setVisibility(View.VISIBLE);
                        isOtroModelo = true;
                    } else {
                        if (!descripcionModelo.equals("Seleccionar")) {
                            getIdModelo(list, spinnerModelo.getItemAtPosition(position).toString());
                        } else {
                            descripcionModelo = "";
                        }
                        etOtroModelo.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            ArrayList<String> listNombreModelo = new ArrayList<>();
            listNombreModelo.add(" ");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, listNombreModelo);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerModelo.setAdapter(dataAdapter);
        }
    }

    private void getIdModelo(ArrayList<ModeloEntity> list, String modelo) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDescripcion().equals(modelo)) {
                idModelo = list.get(i).getIdModelo();
            }
        }
    }

    @Override
    public void getListEmpresa(final ArrayList<OperadoraEntity> list) {
        ArrayList<String> listEmpresa = new ArrayList<>();
        listEmpresa.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listEmpresa.add(list.get(i).getNombreComercial());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listEmpresa);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmpresa.setAdapter(dataAdapter);

        spinnerEmpresa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                descripcionOperadora = spinnerEmpresa.getItemAtPosition(position).toString();
                if (!descripcionOperadora.equals("Seleccionar")) {
                    spinnerEmpresa.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    getIdEmpresaOperadora(list, descripcionOperadora);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getIdEmpresaOperadora(ArrayList<OperadoraEntity> list, String operadora) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getNombreComercial().equals(operadora)) {
                idEmpresOperadora = Integer.valueOf(list.get(i).getCodigoOperador());
            }
        }
    }

    @Override
    public void getListProblema(final ArrayList<ProblemaEntity> list) {
        listProblema = new ArrayList<>();
        listProblema = list;
        if(mProblema!=null){
            mSessionManager.setProblema(mProblema);
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("problemasList", list);
        ProblemasListFragment serviciosListFragment = ProblemasListFragment.newInstance(bundle);
        ActivityUtils.addFragment(getActivity().getSupportFragmentManager(),
                serviciosListFragment, R.id.new_body, "Problema List Fragment");
    }

    public void getProblemaEntity(ProblemaEntity problemaEntity) {
        if (problemaEntity != null) {
            mProblema = problemaEntity;
            etProblemaDetectado.setText(problemaEntity.getDescripcion());
            problema = Integer.valueOf(problemaEntity.getValor());
        } else {
            mSessionManager.setProblema(mProblema);
            if (etProblemaDetectado.getText().toString().isEmpty()) {
                mSessionManager.setProblemaNull();
                mProblema = null;
                Toast.makeText(getContext(), "Deberá seleccionar un tipo de problema para continuar", Toast.LENGTH_SHORT).show();
            }
        }

    }
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ReclamoImeiContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, false, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                remove();
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void getDateTime(final int tipe) {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat;
        final SimpleDateFormat simpleDateFormatText;

        Locale spanish = new Locale("es", "ES");
        simpleDateFormat = new SimpleDateFormat("EEE d MMM", spanish);
        simpleDateFormatText = new SimpleDateFormat("EEE dd MMM hh:mm", spanish);
        final SimpleDateFormat simpleDateFormatTextBD = new SimpleDateFormat("dd/MM/yyyy hh:mm", spanish);

        singleBuilder = new SingleDateAndTimePickerDialog.Builder(getContext())
                .bottomSheet()
                .curved()
                .mainColor(getResources().getColor(R.color.colorPrimary))
                .displayHours(true)
                .displayMinutes(true)
                .displayDays(true)
                .setDayFormatter(simpleDateFormat)
                .maxDateRange(calendar.getTime())
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {

                    }
                })
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        if(date.compareTo(calendar.getTime())>0){
                            getActualDate();
                        }else{
                            if (tipe == 1) {
                                tvFechaReporte.setText(simpleDateFormatText.format(date));
                                fechaReporte = simpleDateFormatTextBD.format(date);
                            } else {
                                tvFechaRecuperacion.setText(simpleDateFormatText.format(date));
                                fechaRecuperacion = simpleDateFormatTextBD.format(date);
                            }
                        }

                        //  singleText.setText(simpleDateFormat.format(date));
                    }
                });
        singleBuilder.display();
    }

    @OnClick({R.id.tv_fecha_reporte, R.id.tv_fecha_recuperacion, R.id.button_send, R.id.et_problema_detectado})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_fecha_reporte:
                getDateTime(1);
                break;
            case R.id.tv_fecha_recuperacion:
                getDateTime(2);
                break;
            case R.id.button_send:
                sendLog(AppConstants.ACTION_SEND_REPORT_IMEI);
                updateUI();
                if(!validarEmail(etCorreoElectronico.getText().toString())){
                    etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                }
                validator.validate();
                break;
            case R.id.et_problema_detectado:
                if(listProblema ==null){
                    mPresenter.getProblemas();
                }else{
                    if(mProblema!=null){
                        mSessionManager.setProblema(mProblema);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("problemasList", listProblema);
                    ProblemasListFragment serviciosListFragment = ProblemasListFragment.newInstance(bundle);
                    ActivityUtils.addFragment(getActivity().getSupportFragmentManager(),
                            serviciosListFragment, R.id.new_body, "Problema List Fragment");
                }
                break;
        }
    }

    public void sendLog(String action){
        bodyEntityLog.setAccion(action);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    private void updateUI() {
        etImei.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etNombres.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etApPaterno.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etApMaterno.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etNumMovil.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        tvErrorImei.setVisibility(View.GONE);
        etProblemaDetectado.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        spinnerEmpresa.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_edittext));
    }

    @Override
    public void onValidationSucceeded() {
        if(!validarEmail(etCorreoElectronico.getText().toString())){
            etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }else{
            if (!descripcionMarca.equals("Seleccionar")) {
                BodyReportarImei bodyReportarImei = new BodyReportarImei();
                bodyReportarImei.setCodigoImei(etImei.getText().toString());
                bodyReportarImei.setEmpresaOperadora(idEmpresOperadora);
                bodyReportarImei.setDescripcionEmpresaOperadora(descripcionOperadora);
                bodyReportarImei.setNombres(etNombres.getText().toString());
                bodyReportarImei.setApellidoPaterno(etApPaterno.getText().toString());
                bodyReportarImei.setApellidoMaterno(etApMaterno.getText().toString());
                if (isOtraMarca) {
                    bodyReportarImei.setIdMarca(0);
                    bodyReportarImei.setDescripcionMarca("Otros");
                } else {
                    bodyReportarImei.setIdMarca(idMarca);
                    bodyReportarImei.setDescripcionMarca(descripcionMarca);
                }

                if (isOtroModelo) {
                    bodyReportarImei.setIdModelo(0);
                    bodyReportarImei.setDescripcionModelo("Otros");
                } else {
                    bodyReportarImei.setIdModelo(idModelo);
                    bodyReportarImei.setDescripcionModelo(descripcionModelo);
                }

                bodyReportarImei.setNumeroMovil(etNumMovil.getText().toString());
                bodyReportarImei.setFechaReporte(fechaReporte);
                bodyReportarImei.setFechaRecuperacion(fechaRecuperacion);
                bodyReportarImei.setNumeroContacto(etNumContacto.getText().toString());
                bodyReportarImei.setProblemaDetectado(problema);
                bodyReportarImei.setCodigoBloqueo(etCodigoBloqueo.getText().toString());
                bodyReportarImei.setCorreoElectronico(etCorreoElectronico.getText().toString());
                bodyReportarImei.setGuid(mSessionManager.getMac());
                bodyReportarImei.setSistemaOperativo("Android");
                bodyReportarImei.setModeloCelular(Build.MODEL);
                bodyReportarImei.setMarcaCelular(Build.BRAND);
                if (mSessionManager.getUserEntity() != null) {
                    bodyReportarImei.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
                    bodyReportarImei.setNúmeroCelular(mSessionManager.getUserEntity().getNroTelefono());
                } else {
                    bodyReportarImei.setNombreUsuario(" ");
                    bodyReportarImei.setNúmeroCelular(" ");
                }
                bodyReportarImei.setMac(mSessionManager.getMac());
                mPresenter.sendReport(bodyReportarImei);
            } else {
                if (descripcionMarca.equals("Seleccionar")) {
                    spinnerMarca.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                    Toast.makeText(getContext(), "Ingrese una marca válida", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        String msg = "Por favor ingrese sus datos correctamente";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                if (view.getId() == R.id.et_imei) {
                    if (!etImei.getText().toString().isEmpty()) {
                        tvErrorImei.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (view instanceof Spinner) {
                    if (view.getId() == R.id.spinner_empresa) {
                        if (descripcionOperadora.equals("Seleccionar")) {
                            spinnerEmpresa.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }
                    if (view.getId() == R.id.spinner_marca) {
                        if (descripcionMarca.equals("Seleccionar")) {
                            spinnerMarca.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }

                } else {
                    if (view instanceof TextView) {
                        if (view.getId() == R.id.et_problema_detectado) {
                            etProblemaDetectado.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }
                }
                //Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mSessionManager.setProblemaNull();
        if(singleBuilder!=null){
            singleBuilder.dismiss();
        }
    }

}
