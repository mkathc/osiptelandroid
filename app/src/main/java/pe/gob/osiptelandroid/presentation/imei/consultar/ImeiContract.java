package pe.gob.osiptelandroid.presentation.imei.consultar;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.ConsultaImeiEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface ImeiContract {
    interface View extends BaseView<Presenter> {

        void imeiSuccess(ArrayList<ConsultaImeiEntity> consultaImeiEntity);

        void imeiFailure(String message);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void sendImei(String imei, String nombreUsuario, String numeroCel);
        void getRefreshToken();

    }
}
