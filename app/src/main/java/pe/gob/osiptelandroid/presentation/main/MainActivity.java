package pe.gob.osiptelandroid.presentation.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseActivity;
import pe.gob.osiptelandroid.data.entities.CoberturaByOperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaFragment;
import pe.gob.osiptelandroid.presentation.expedientes.misexpedientes.ExpedientesFragment;
import pe.gob.osiptelandroid.presentation.expedientes.registro_trasu.RegistroTrasufragment;
import pe.gob.osiptelandroid.presentation.expedientes.registro_trasu.ServiciosTrasuListFragment;
import pe.gob.osiptelandroid.presentation.guia.GuiaFragment;
import pe.gob.osiptelandroid.presentation.imei.consultar.ImeiFragment;
import pe.gob.osiptelandroid.presentation.imei.consultar.ImeiListFragment;
import pe.gob.osiptelandroid.presentation.imei.reclamo.ProblemasListFragment;
import pe.gob.osiptelandroid.presentation.imei.reclamo.ReclamoImeiFragment;
import pe.gob.osiptelandroid.presentation.oficinas.OficinasFragment;
import pe.gob.osiptelandroid.presentation.expedientes.ConsultarExpedienteFragment;
import pe.gob.osiptelandroid.presentation.signal.SignalFragment;
import pe.gob.osiptelandroid.presentation.signal.consultar.ConsultarSignalFragment;
import pe.gob.osiptelandroid.presentation.signal.reportar.MapaReportarSignalFragment;
import pe.gob.osiptelandroid.presentation.signal.reportar.ReportarSignalFragment;
import pe.gob.osiptelandroid.presentation.signal.reportar.ServiciosListFragment;
import pe.gob.osiptelandroid.presentation.verficarlinea.VerificarLineaFragment;
import pe.gob.osiptelandroid.services.CounterService;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.BottomNavigationViewHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;

/**
 * Created by Kath on 12/12/17.
 */

public class MainActivity extends BaseActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        ImeiListFragment.activateButtons, ReclamoImeiFragment.activateButtons, ExpedientesFragment.activateButtonsExpedientes,
        RegistroTrasufragment.activateButtonsExpedientes, GuiaFragment.activateMenu,
        VerificarLineaFragment.activateMenu, EncuestaFragment.activateMenu, ServiciosTrasuListFragment.sendServiceTrasuInterface,
        MapaReportarSignalFragment.isCosultarUpdateView, ProblemasListFragment.sendDataReportImeiInterface,
        ConsultarSignalFragment.sendConsultaCobertura, SignalFragment.bottomHiden, ServiciosListFragment.sendDataToReportInterface {

    private static final int MENU_HOME = 1;
    private static final int MENU_IMEI = 2;
    private static final int MENU_RECORDS = 3;
    private static final int MENU_SIGNAL = 4;
    private static final int MENU_OFFICES = 5;

    private SessionManager mSessionManager;
    private BroadcastReceiver mCounterReceiver;
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.contentContainer_1)
    FrameLayout contentContainer1;
    @BindView(R.id.contentContainer_2)
    FrameLayout contentContainer2;
    @BindView(R.id.contentContainer_3)
    FrameLayout contentContainer3;
    @BindView(R.id.contentContainer_4)
    FrameLayout contentContainer4;
    @BindView(R.id.contentContainer_5)
    FrameLayout contentContainer5;

    ImeiFragment imeiFragment;
    ConsultarExpedienteFragment consultarExpedienteFragment;
    SignalFragment señalFragment;
    OficinasFragment officesFragment;
    private String pStatus;
    private boolean isSignal = false;
    private boolean updateCobertura = false;
    private boolean isFirstSignal = true;
    private boolean isFirstOffices = true;
    private boolean isFirstConsult = true;
    private boolean isFirstExpedientes = true;
    private boolean isFirstActivate = true;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;

    @Override
    protected void onResume() {
        super.onResume();
        if (mSessionManager.getLoginTrasu()) {
            stopService(new Intent(getApplicationContext(), CounterService.class));
            mSessionManager.setStatus(30000);
        }
        if (mCounterReceiver == null) {
            mCounterReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    pStatus = intent.getExtras().getString("count");

                    Log.e("TIMER", pStatus);

                    if (pStatus.equals("01")) {
                        mSessionManager.setLoginTrasu(false);
                        if (mSessionManager.getUserTrasu() != null) {
                            UserTrasuEntity userTrasuEntity = new UserTrasuEntity();
                            userTrasuEntity = mSessionManager.getUserTrasu();
                            userTrasuEntity.setNroDoc("");
                            mSessionManager.setUserTrasu(userTrasuEntity);
                        }
                        stopService(new Intent(getApplicationContext(), CounterService.class));
                        mSessionManager.setStatus(30000);
                    }
                }
            };
        }

        registerReceiver(mCounterReceiver, new IntentFilter("count"));

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mSessionManager.getLoginTrasu()) {
            stopService(new Intent(getApplicationContext(), CounterService.class));
            mSessionManager.setStatus(30000);
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenterLog = new LogPresenter( this);
        bodyEntityLog = new BodyEntityLog();

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomBar);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        mSessionManager = new SessionManager(getApplicationContext());

        selectedFragment(MENU_HOME);

        MenuFragment fragment = (MenuFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_1);

        if (fragment == null) {
            fragment = MenuFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.contentContainer_1);
        }


        imeiFragment = (ImeiFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_2);

        if (imeiFragment == null) {
            imeiFragment = ImeiFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    imeiFragment, R.id.contentContainer_2);
        }

        consultarExpedienteFragment = (ConsultarExpedienteFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_3);

        if (consultarExpedienteFragment == null) {
            consultarExpedienteFragment = ConsultarExpedienteFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    consultarExpedienteFragment, R.id.contentContainer_3);
        }

        señalFragment = (SignalFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_4);

        if (señalFragment == null) {
            señalFragment = SignalFragment.newInstance();

            ActivityUtils.addSignalFragment(getSupportFragmentManager(),
                    señalFragment, R.id.contentContainer_4);
        }

        officesFragment = (OficinasFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_5);

        if (officesFragment == null) {
            officesFragment = OficinasFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), officesFragment, R.id.contentContainer_5);
        }

    }


    private void selectedFragment(int option) {
        contentContainer1.setVisibility(option == MENU_HOME ? View.VISIBLE : View.GONE);
        contentContainer2.setVisibility(option == MENU_IMEI ? View.VISIBLE : View.GONE);
        contentContainer3.setVisibility(option == MENU_RECORDS ? View.VISIBLE : View.GONE);
        contentContainer4.setVisibility(option == MENU_SIGNAL ? View.VISIBLE : View.GONE);
        contentContainer5.setVisibility(option == MENU_OFFICES ? View.VISIBLE : View.GONE);

    }

    public void setConsultImei() {
        selectedFragment(MENU_IMEI);
        bottomNavigationView.setSelectedItemId(R.id.id_imei);
    }

    public void setCosultOffices() {
        selectedFragment(MENU_OFFICES);
        bottomNavigationView.setSelectedItemId(R.id.id_offices);
    }

    public void setCosultRecords() {
        selectedFragment(MENU_RECORDS);
        bottomNavigationView.setSelectedItemId(R.id.id_records);
    }

    public void setCosultSignal() {
        selectedFragment(MENU_SIGNAL);
        bottomNavigationView.setSelectedItemId(R.id.id_signal);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.id_home:
                imeiFragment.deleteImei();
                consultarExpedienteFragment.cleanView();
                selectedFragment(MENU_HOME);
                break;
            case R.id.id_imei:
                consultarExpedienteFragment.cleanView();
                selectedFragment(MENU_IMEI);
                sendLog(AppConstants.ACTION_GETIN_CONSULT_IMEI);
                break;
            case R.id.id_records:
                imeiFragment.deleteImei();
                selectedFragment(MENU_RECORDS);
                sendLog(AppConstants.ACTION_GETIN_TRASU_PROCEEDINGS);
                break;
            case R.id.id_signal:
                isSignal = true;
                if (isFirstSignal) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            //locationStart();
                            señalFragment.updateLocation();

                        } else {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        }
                    }
                    isFirstSignal = false;
                } else {
                    señalFragment.setMapToCenter();
                }
                imeiFragment.deleteImei();
                consultarExpedienteFragment.cleanView();
                selectedFragment(MENU_SIGNAL);
                sendLog(AppConstants.ACTION_GETIN_OSIPTEL_SIGNAL);
                break;
            case R.id.id_offices:
                isSignal = false;
                if (isFirstOffices) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            //locationStart();
                            officesFragment.updateLocation();
                        } else {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        }
                    }
                    isFirstOffices = false;
                } else {
                    officesFragment.setMapToCenter();
                }
                imeiFragment.deleteImei();
                consultarExpedienteFragment.cleanView();
                selectedFragment(MENU_OFFICES);
                sendLog(AppConstants.ACTION_GETIN_OUR_OFFICES);
                break;
            default:
                break;
        }
        return true;

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Habilitar GPS")
                        .setMessage("Se requieren permisos de gps")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        //mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        // mMap.setMyLocationEnabled(true);
                        if (isSignal) {
                            señalFragment.updateLocation();
                        } else {
                            officesFragment.updateLocation();
                        }
                    }
                } else {
                    if (isSignal) {
                        señalFragment.setMapToCenter();
                    } else {
                        officesFragment.setMapToCenter();
                    }
                }
                break;
            }
        }
    }

    private void sendLog(String action){
        bodyEntityLog.setAccion(action);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }

    @Override
    public void sendActivateImeiButtons(Boolean isActive) {
        ImeiFragment imeiFragment = (ImeiFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_2);
        imeiFragment.activateButtons(isActive);
    }

    @Override
    public void sendActivateExpedientesButtons(Boolean isActive) {
        ConsultarExpedienteFragment consultarExpedienteFragment = (ConsultarExpedienteFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_3);
        consultarExpedienteFragment.activateButtons(isActive);
    }

    @Override
    public void sendActivateMenu(Boolean isActive) {
        MenuFragment fragment = (MenuFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_1);
        fragment.activateMenu(isActive);
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        int size = getSupportFragmentManager().getFragments().size();

        String tag = getSupportFragmentManager().getFragments().get(size - 1).getTag();
        String tag2 = getSupportFragmentManager().getFragments().get(size - 2).getTag();
        Log.e("FRAGMENT COUNT", "--------------------->  " + String.valueOf(count));
        Log.e("FRAGMENT LIST", "--------------------->  " + String.valueOf(size));
        Log.e("TAG FRAGMENT 1", "--------------------->  " + tag);
        Log.e("TAG FRAGMENT 2", "--------------------->  " + tag2);

        if (count == 0) {
            switch (tag) {
                case "Home":
                    sendBottomHiden(false);
                    if (updateCobertura) {
                        señalFragment.updateBack();
                        updateCobertura = false;
                    } else {
                        if (bottomNavigationView.getSelectedItemId() == R.id.id_home) {
                            finish();
                        } else {
                            selectedFragment(MENU_HOME);
                            bottomNavigationView.setSelectedItemId(R.id.id_home);
                        }
                    }
                    break;
                case "ActivateMenu":
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_home) {
                        super.onBackPressed();
                        sendActivateMenu(true);
                    } else {
                        if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                            if (isFirstConsult) {
                                sendBottomHiden(false);
                                sendCleanView(true);
                                isFirstConsult = false;
                            } else {
                                sendBottomHiden(false);
                                sendCleanView(true);
                                selectedFragment(MENU_HOME);
                                bottomNavigationView.setSelectedItemId(R.id.id_home);
                            }
                        } else {
                            selectedFragment(MENU_HOME);
                            bottomNavigationView.setSelectedItemId(R.id.id_home);
                        }
                    }

                    break;
                case "ActivateImei":
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_imei) {
                        super.onBackPressed();
                        sendActivateImeiButtons(true);
                        if (mSessionManager.getProblema() != null) {
                            mSessionManager.setProblemaNull();
                        }
                    } else {
                        if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                            if (isFirstActivate) {
                                sendBottomHiden(false);
                                sendCleanView(true);
                                isFirstActivate = false;
                            } else {
                                sendBottomHiden(false);
                                sendCleanView(true);
                                selectedFragment(MENU_HOME);
                                bottomNavigationView.setSelectedItemId(R.id.id_home);
                            }
                        } else {
                            selectedFragment(MENU_HOME);
                            bottomNavigationView.setSelectedItemId(R.id.id_home);
                        }
                    }
                    break;
                case "Consultar Signal":
                    sendBottomHiden(false);
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                        super.onBackPressed();
                    } else {
                        selectedFragment(MENU_HOME);
                        bottomNavigationView.setSelectedItemId(R.id.id_home);
                    }
                    break;
                case "Reportar Signal":
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                        super.onBackPressed();
                    } else {
                        selectedFragment(MENU_HOME);
                        bottomNavigationView.setSelectedItemId(R.id.id_home);
                    }
                    break;
                case "Mapa Signal":
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                        super.onBackPressed();
                    } else {
                        selectedFragment(MENU_HOME);
                        bottomNavigationView.setSelectedItemId(R.id.id_home);
                    }
                    break;
                case "ActivateExpedientes":
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_records) {
                        super.onBackPressed();
                        sendActivateExpedientesButtons(true);
                        if (mSessionManager.getServiceTrasu() != null) {
                            mSessionManager.setServiceTrasuNull();
                        }
                    } else {
                        if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                            if (isFirstExpedientes) {
                                sendBottomHiden(false);
                                sendCleanView(true);
                                isFirstExpedientes = false;
                            } else {
                                sendBottomHiden(false);
                                sendCleanView(true);
                                selectedFragment(MENU_HOME);
                                bottomNavigationView.setSelectedItemId(R.id.id_home);
                            }
                        } else {
                            selectedFragment(MENU_HOME);
                            bottomNavigationView.setSelectedItemId(R.id.id_home);
                        }
                    }
                    break;
                case "CloseSession":

                    if (bottomNavigationView.getSelectedItemId() == R.id.id_records) {
                        ExpedientesFragment fragment = (ExpedientesFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.body_consultar);
                        fragment.closeDialog();
                    } else {
                        if (bottomNavigationView.getSelectedItemId() == R.id.id_signal) {
                            sendBottomHiden(false);
                            sendCleanView(true);
                        } else {
                            selectedFragment(MENU_HOME);
                            bottomNavigationView.setSelectedItemId(R.id.id_home);
                        }
                    }
                    break;
                case "Signal":
                    sendBottomHiden(false);
                    if (bottomNavigationView.getSelectedItemId() == R.id.id_home) {
                        finish();
                    } else {
                        selectedFragment(MENU_HOME);
                        bottomNavigationView.setSelectedItemId(R.id.id_home);
                    }
                    break;
                case "Problema List Fragment":
                    mSessionManager.setProblemaNull();
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag("Problema List Fragment");
                    ActivityUtils.removeFragment(getSupportFragmentManager(), fragment);
                    break;
                case "Servicio Trasu List Fragment":
                    mSessionManager.setServiceTrasuNull();
                    Fragment fragmentTrasu = getSupportFragmentManager().findFragmentByTag("Servicio Trasu List Fragment");
                    ActivityUtils.removeFragment(getSupportFragmentManager(), fragmentTrasu);
                    break;
                case "Servicios List Fragment":
                    mSessionManager.getArrayListaServiciosOperadores(null);
                    Fragment fragmentOperadores = getSupportFragmentManager().findFragmentByTag("Servicios List Fragment");
                    ActivityUtils.removeFragment(getSupportFragmentManager(), fragmentOperadores);
                    ReportarSignalFragment reportarSignalFragment = (ReportarSignalFragment) getSupportFragmentManager().findFragmentByTag("Reportar Signal");
                    reportarSignalFragment.getDataListServicios(null);
                    break;
                default:
                    MenuFragment fragmentmenu = (MenuFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.contentContainer_1);
                    fragmentmenu.setGps();
                    super.onBackPressed();
                    break;
            }
        } else {
            sendBottomHiden(false);
            selectedFragment(MENU_HOME);
            bottomNavigationView.setSelectedItemId(R.id.id_home);
        }
    }

    @Override
    public void sendConsultaCoberturaUI(ArrayList<CoberturaByOperadoraEntity> list, String Dpto, String prov, String distr, String localidad) {
        SignalFragment señalFragment = (SignalFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentContainer_4);
        señalFragment.updateUiConsultaCobertura(list);
        señalFragment.getDataLocation(Dpto, prov, distr, localidad);
        updateCobertura = true;
    }


    @Override
    public void sendBottomHiden(Boolean isHiden) {
        if (isHiden) {
            bottomNavigationView.setVisibility(View.GONE);
        } else {
            bottomNavigationView.setVisibility(View.VISIBLE);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSessionManager.getLoginTrasu()) {
            Intent serviceIntent = new Intent(this, CounterService.class);
            startService(serviceIntent);
        }

        //Clean SharedPreferences
        mSessionManager.saveArrayListaServiciosOperadores(null);
        mSessionManager.saveArrayListDepartamentos(null);
        mSessionManager.saveArrayListaServiciosOperadores(null);
        mSessionManager.saveArrayListSendOperadoresCobertura(null);
        mSessionManager.saveArrayList(null);
        mSessionManager.setProblemaNull();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSessionManager.getLoginTrasu()) {
            Intent serviceIntent = new Intent(this, CounterService.class);
            startService(serviceIntent);
        }
    }

    @Override
    public void sendDataToReport(ArrayList<ServicioOperadorEntity> list) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("Servicios List Fragment");
        ActivityUtils.removeFragment(getSupportFragmentManager(), fragment);
        ReportarSignalFragment reportarSignalFragment = (ReportarSignalFragment) getSupportFragmentManager().findFragmentByTag("Reportar Signal");
        reportarSignalFragment.getDataListServicios(list);
    }

    @Override
    public void sendCleanView(Boolean cleanView) {
        if (cleanView) {
            SignalFragment señalFragment = (SignalFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.contentContainer_4);
            señalFragment.updateBack();
        }
    }

    @Override
    public void sendDataToReportImei(ProblemaEntity problemaEntity) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("Problema List Fragment");
        ActivityUtils.removeFragment(getSupportFragmentManager(), fragment);
        ReclamoImeiFragment reclamoImeiFragment = (ReclamoImeiFragment) getSupportFragmentManager().findFragmentByTag("ActivateImei");
        reclamoImeiFragment.getProblemaEntity(problemaEntity);
    }

    @Override
    public void sendServiceTrasu(ServiceEntity serviceEntity) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("Servicio Trasu List Fragment");
        ActivityUtils.removeFragment(getSupportFragmentManager(), fragment);
        RegistroTrasufragment registroTrasufragment = (RegistroTrasufragment) getSupportFragmentManager().findFragmentByTag("ActivateExpedientes");
        registroTrasufragment.getTypeSereviceTrasu(serviceEntity);
    }
}

