package pe.gob.osiptelandroid.presentation.signal.dialog;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComentarioDialog extends AlertDialog {
    @BindView(R.id.im_close)
    ImageView imClose;
    @BindView(R.id.message)
    TextView message;

    public ComentarioDialog(Context context, String text) {
        super(context);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_comentario, null);
        setView(view);
        ButterKnife.bind(this, view);
        message.setText(text);
    }

    @OnClick(R.id.im_close)
    public void onViewClicked() {
        dismiss();
    }
}
