package pe.gob.osiptelandroid.presentation.register;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.UserEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyUser;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.PostRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

    private RegisterContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public RegisterPresenter(RegisterContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void sendRegister(BodyUser bodyUser) {
      //  mView.showMessage(imei);
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createService(PostRequest.class);
        Call<ArrayList<UserEntity>> orders = postRequest.sendRegister("Bearer "+mSessionManager.getUserToken(),bodyUser);
        orders.enqueue(new Callback<ArrayList<UserEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<UserEntity>> call, Response<ArrayList<UserEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if(response.body().size()!=0){
                        mSessionManager.openSession(response.body().get(0));
                        mView.showMessage("Se ha registrado con éxito");
                    }
                } else {
                    mView.showErrorMessage("Error al realizar el registro, intente nuevamente porfavor");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
