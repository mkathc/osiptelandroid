package pe.gob.osiptelandroid.presentation.splash;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseActivity;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.main.MainActivity;
import pe.gob.osiptelandroid.presentation.splash.dialogs.NotConnectionDialog;
import pe.gob.osiptelandroid.presentation.splash.dialogs.SplashInterface;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;

/**
 * Created by junior on 27/08/16.
 */
public class SplashActivity extends BaseActivity implements InitialContract.View, SplashInterface {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final long SPLASH_SCREEN_DELAY = 2000;
    private static final int MY_REQUEST_CODE = 120;
    private InitialContract.Presenter mPresenter;
    private SessionManager mSessionManager;
    private NotConnectionDialog notConnectionDialog;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;
    public SplashActivity() {
        // Requires empty public constructor
    }

    public static SplashActivity newInstance() {
        return new SplashActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppUpdateManager updateManager = AppUpdateManagerFactory.create(this);

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = updateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() ==
                    UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                Log.e("Update", "Update");
                // Request the update.
                try {
                    updateManager.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.IMMEDIATE,
                            this,
                            MY_REQUEST_CODE);
                    Log.e("Start Update", "Start Update");

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);
        // Creates instance of the manager.
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(this);

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // For a flexible update, use AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                Log.e("Update", "Update");
                // Request the update.
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            AppUpdateType.IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            MY_REQUEST_CODE);
                    Log.e("Start Update", "Start Update");

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }else{
                Log.e("Not Update", "Not Update");

            }
        });

        AppCenter.start(getApplication(), "cae134fe-f7d3-4a2b-a358-041302997bf0", Analytics.class, Crashes.class);
        mPresenter = new InitialPresenter(this, getApplicationContext());
        mSessionManager = new SessionManager(getApplicationContext());
        mSessionManager.setGPSstatus(true);
        mPresenterLog = new LogPresenter( getApplicationContext());
        mSessionManager = new SessionManager( getApplicationContext() );
        bodyEntityLog = new BodyEntityLog();
        mPresenter.getToken("OsitelApp", "d60d6b88-bf36-463f-ae90-b423188bf1b", "client_credentials");
    }

    private void initMain() {
        bodyEntityLog.setAccion(AppConstants.ACTION_GETIN_APP);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
        next(this, null, MainActivity.class, true);
    }

    private void initialProcess() {
        if (mSessionManager.isLogin()) {
            mPresenter.getEncuesta(mSessionManager.getUserEntity().getIdUsuario());
          //  next(this, null, MainActivity.class, true);
        } else {
            next(this, null, InitialActivity.class, true);
        }
    }

    @Override
    public void sendLogError(String error) {
        bodyEntityLog.setAccion("ERROR: " + error);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getNombreCompleto());
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    @Override
    public void getUserResponse() {
        initialProcess();
    }

    @Override
    public void getUserToken(AccessTokenEntity accessTokenEntity) {
        mSessionManager.setUserToken(accessTokenEntity.getAccess_token());
        if (mSessionManager.isLogin()) {
            mPresenter.getEncuesta(mSessionManager.getUserEntity().getIdUsuario());
            Log.e("is login", "SI");
        } else {
            Log.e("is login", "NO");
            String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            mPresenter.sendMac(androidId);
        }

        mPresenter.getTimeSession();
        mSessionManager.saveArrayList(null);
    }

    @Override
    public void getMyEncuestaResponse() {
        initMain();
    }

    @Override
    public void getMyEncuestaResponseError() {
        next(this, null, InitialActivity.class, true);
    }

    @Override
    public void getNotConnectionError() {
        notConnectionDialog = new NotConnectionDialog(this, this);
        notConnectionDialog.show();
        notConnectionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        notConnectionDialog.setCancelable(false);
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void setPresenter(InitialContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
    }

    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void resend() {
        notConnectionDialog.dismiss();
        mPresenter.getToken("OsitelApp", "d60d6b88-bf36-463f-ae90-b423188bf1b", "client_credentials");
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            if (requestCode != RESULT_OK) {
                Log.e("Update flow failed!", String.valueOf(resultCode));

                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Atención!");
                alertDialog.setMessage("Es necesario actualizar para poder continuar");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "ACEPTAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                alertDialog.show();

                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }
}
