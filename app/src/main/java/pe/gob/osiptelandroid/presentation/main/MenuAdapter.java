package pe.gob.osiptelandroid.presentation.main;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.MenuEntity;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class MenuAdapter extends LoaderAdapter<MenuEntity> implements OnClickListListener {



    private Context context;
    private MenuItem menuItem;

    public MenuAdapter(ArrayList<MenuEntity> menuEntities, Context context, MenuItem menuItem) {
        super(context);
        setItems(menuEntities);
        this.context = context;
        this.menuItem = menuItem;

    }

    public ArrayList<MenuEntity> getItems() {
        return (ArrayList<MenuEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getId();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        return new ViewHolder(root, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        MenuEntity menuEntity = getItems().get(position);
        ((ViewHolder) holder).tvName.setText(menuEntity.getName());
        ((ViewHolder) holder).imIcon.setImageDrawable(menuEntity.getIcon());
        if(menuEntity.getId() == 2){
            ((ViewHolder) holder).imIcon.getLayoutParams().height = 100;
            ((ViewHolder) holder).imIcon.getLayoutParams().width = 200;
        }
    }



    @Override
    public void onClick(int position) {
        //Open Fragment
        MenuEntity menuEntity = getItems().get(position);
        menuItem.clickItem(menuEntity);
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.im_icon)
        ImageView imIcon;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.rl_menu)
        RelativeLayout rlMenu;

        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
