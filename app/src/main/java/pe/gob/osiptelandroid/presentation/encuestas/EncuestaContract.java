package pe.gob.osiptelandroid.presentation.encuestas;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.body.BodyEncuestaEntity;
import pe.gob.osiptelandroid.data.entities.PreguntaEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface EncuestaContract {
    interface View extends BaseView<Presenter> {
        void getListPreguntas(ArrayList<PreguntaEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getPreguntas();
        void setEncuesta(BodyEncuestaEntity encuesta);
        void getRefreshToken();

      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
