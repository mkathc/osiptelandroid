package pe.gob.osiptelandroid.presentation.verficarlinea;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class VerificarPresenter implements VerificarContract.Presenter {

    private VerificarContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public VerificarPresenter(VerificarContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getOperadores() {
      //  mView.showMessage(imei);
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<OperadoraEntity>> orders = listRequest.getOperadores("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<OperadoraEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<OperadoraEntity>> call, Response<ArrayList<OperadoraEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListOperadores(response.body());

                } else {
                    APIError apiError = ErrorUtils.firstParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de operadores, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }                }
            }

            @Override
            public void onFailure(Call<ArrayList<OperadoraEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getOperadores();
                } else {
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

}


   /* @Override
    public void clickItem(RestEntinty restEntinty) {
        mView.clickItemRestaurante(restEntinty);
    }

    @Override
    public void deleteItem(RestEntinty restEntinty, int position) {

    }

    @Override
    public void loadOrdersFromPage(int page, int id) {
        getRestaurante(page, id);

    }

    @Override
    public void loadfromNextPage(int id) {

        if (currentPage > 0)
            getRestaurante(currentPage, id);

    }

    @Override
    public void startLoad(int id) {
        if (!firstLoad) {
            firstLoad = true;
            loadOrdersFromPage(1, id);
        }
    }

    @Override
    public void getRestaurante(final int page, int id) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<TrackEntityHolder<RestEntinty>> orders = listRequest.getListRestaurantes("Token "+mSessionManager.getUserToken(),id ,page);
        orders.enqueue(new Callback<TrackEntityHolder<RestEntinty>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<RestEntinty>> call, Response<TrackEntityHolder<RestEntinty>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.getListRestaurante(response.body().getResults());

                    if (response.body().getNext() != null) {
                        currentPage = page +1;
                    } else {
                        currentPage = -1;
                    }

                } else {
                    mView.showErrorMessage("Error al obtener las órdenes");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<RestEntinty>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }*/
