package pe.gob.osiptelandroid.presentation.register;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.body.BodyUser;

/**
 * Created by katherine on 12/05/17.
 */

public interface RegisterContract {
    interface View extends BaseView<Presenter> {

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void sendRegister(BodyUser bodyUser);

      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
