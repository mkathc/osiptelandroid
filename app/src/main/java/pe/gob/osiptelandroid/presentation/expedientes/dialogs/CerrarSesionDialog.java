package pe.gob.osiptelandroid.presentation.expedientes.dialogs;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CerrarSesionDialog extends AlertDialog {


    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.aceptar)
    Button aceptar;
    @BindView(R.id.im_close)
    ImageView imClose;
    private Context mContext;


    private ExpedientesInterface expedientesInterface;

    public CerrarSesionDialog(Context context, ExpedientesInterface expedientesInterface) {
        super(context);
        this.expedientesInterface = expedientesInterface;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_close_session, null);
        setView(view);
        ButterKnife.bind(this, view);
        mContext = context;
    }


    @OnClick({R.id.im_close, R.id.cancel, R.id.aceptar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.im_close:
                dismiss();
                break;
            case R.id.cancel:
                dismiss();
                cancel.setTextColor(mContext.getResources().getColor(R.color.white));
                break;
            case R.id.aceptar:
                expedientesInterface.closeSession();
                aceptar.setTextColor(mContext.getResources().getColor(R.color.white));
                break;
        }
    }
}
