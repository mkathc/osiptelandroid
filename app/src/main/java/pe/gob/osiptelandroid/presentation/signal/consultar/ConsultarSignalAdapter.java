package pe.gob.osiptelandroid.presentation.signal.consultar;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.verficarlinea.VerificarLineaItem;
import pe.gob.osiptelandroid.utils.OnClickListListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class ConsultarSignalAdapter extends LoaderAdapter<OperadoraEntity> implements OnClickListListener {



    private Context context;
    private VerificarLineaItem verificarLineaItem;
    private ArrayList<String> list;
    private SessionManager mSessionManager;

    public ConsultarSignalAdapter(ArrayList<OperadoraEntity> verificarLineaEntities, Context context, VerificarLineaItem verificarLineaItem) {
        super(context);
        setItems(verificarLineaEntities);
        this.context = context;
        list = new ArrayList<>();
        this.verificarLineaItem = verificarLineaItem;
        mSessionManager = new SessionManager(context);

    }

    public ArrayList<OperadoraEntity> getItems() {
        return (ArrayList<OperadoraEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdOperador();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_consultar_cobertura, parent, false);
        return new ViewHolder(root, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        final OperadoraEntity verificarLineaEntity = getItems().get(position);
        Picasso.get().load(verificarLineaEntity.getNombreImagen()).into(((ConsultarSignalAdapter.ViewHolder) holder).imLogo);
        ((ViewHolder) holder).imSwitch.setChecked(true);
        list.add(verificarLineaEntity.getCodigoOperador());
        mSessionManager.saveArrayListSendOperadoresCobertura(list);
        ((ViewHolder) holder).imSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    list.add(verificarLineaEntity.getCodigoOperador());
                    mSessionManager.saveArrayListSendOperadoresCobertura(list);
                }else{
                    ArrayList<String> newList = new ArrayList<>();
                    newList = mSessionManager.getArrayListSendOperadoresCobertura(SessionManager.ARRAY_SEND_OPERADORES);
                    for (int i = 0; i <newList.size() ; i++) {
                        if(newList.get(i).equals(verificarLineaEntity.getCodigoOperador())){
                            newList.remove(i);
                        }
                    }
                    list = newList;
                    mSessionManager.saveArrayListSendOperadoresCobertura(list);
                }
            }
        });
        //Glide.with(context).load(verificarLineaEntity.getNombreImagen()).override(150, 80).into(((ViewHolder) holder).imLogo);
    }


    @Override
    public void onClick(int position) {
        //Open Fragment
        //OperadoraEntity verificarLineaEntity = getItems().get(position);
        //verificarLineaItem.clickItem(verificarLineaEntity);
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.im_logo)
        ImageView imLogo;
        @BindView(R.id.im_switch)
        Switch imSwitch;
        @BindView(R.id.tv_name_operadora)
        TextView tvNameOperadora;

        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
