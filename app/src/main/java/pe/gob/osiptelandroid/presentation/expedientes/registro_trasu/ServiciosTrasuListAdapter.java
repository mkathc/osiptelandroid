package pe.gob.osiptelandroid.presentation.expedientes.registro_trasu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class ServiciosTrasuListAdapter extends LoaderAdapter<ServiceEntity> {

    private Context context;
    private SessionManager mSessionManager;
    private ServiceEntity mServiceEntity;
    private int pos;
    private int row_index;
    private ArrayList<ServiceEntity> mList;
    private boolean isSelected = false;

    public ServiciosTrasuListAdapter(ArrayList<ServiceEntity> serviceEntities, Context context, ServiceEntity serviceEntity ) {
        super(context);
        setItems(serviceEntities);
        this.context = context;
        mSessionManager = new SessionManager(context);
        this.mServiceEntity = serviceEntity;
        mList = serviceEntities;
    }

    public ArrayList<ServiceEntity> getItems() {
        return (ArrayList<ServiceEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name_problemas, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ServiceEntity serviceEntity = getItems().get(position);
        if (mServiceEntity!= null) {
            if (mServiceEntity.getDescripcion().equals(serviceEntity.getDescripcion())){
                ((ViewHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.colorAccentOff));
                ((ViewHolder) holder).tvNameProblema.setTextColor(context.getResources().getColor(R.color.white));
            }
        }

        ((ViewHolder) holder).tvNameProblema.setText(serviceEntity.getDescripcion());

        ((ViewHolder) holder).container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.colorAccentOff));
                ((ViewHolder) holder).tvNameProblema.setTextColor(context.getResources().getColor(R.color.white));
                row_index = position;
                notifyDataSetChanged();
                mSessionManager.setServiceTrasu(serviceEntity);
                isSelected = true;
            }
        });

        if(isSelected){
            if (row_index == position) {
                ((ViewHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.colorAccentOff));
                ((ViewHolder) holder).tvNameProblema.setTextColor(context.getResources().getColor(R.color.white));

            } else {
                ((ViewHolder) holder).container.setBackgroundColor(context.getResources().getColor(R.color.white));
                ((ViewHolder) holder).tvNameProblema.setTextColor(context.getResources().getColor(R.color.black));
            }
        }

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name_problema)
        TextView tvNameProblema;
        @BindView(R.id.container)
        LinearLayout container;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
