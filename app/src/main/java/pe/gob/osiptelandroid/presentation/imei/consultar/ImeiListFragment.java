package pe.gob.osiptelandroid.presentation.imei.consultar;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ConsultaImeiEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.ActivityUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ImeiListFragment extends BaseFragment {

    private static final String TAG = ImeiListFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    Unbinder unbinder;

    private ImeiAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private SessionManager mSessionManager;
    private ArrayList<ConsultaImeiEntity> list;

    private activateButtons mCallback;

    public interface activateButtons{
        void sendActivateImeiButtons(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (activateButtons) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }


    public ImeiListFragment() {
        // Requires empty public constructor
    }

    public static ImeiListFragment newInstance(Bundle bundle) {
        ImeiListFragment fragment = new ImeiListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.list_imei_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        list = (ArrayList<ConsultaImeiEntity>) getArguments().get(ImeiFragment.LISTA_IMEI);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });

        return root;
    }

    private void remove() {
        mCallback.sendActivateImeiButtons(true);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(), this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mAdapter = new VerReportesAdapter(getList());
        mAdapter = new ImeiAdapter(new ArrayList<ConsultaImeiEntity>(), getContext());
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
        mAdapter.setItems(list);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*@Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        mAdapter.onRestoreInstanceState(savedInstanceState);
    }*/

    //x4yf7z03023
}
