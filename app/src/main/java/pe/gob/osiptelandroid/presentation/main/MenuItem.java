package pe.gob.osiptelandroid.presentation.main;

import pe.gob.osiptelandroid.data.entities.MenuEntity;

/**
 * Created by katherine on 24/04/17.
 */

public interface MenuItem {

    void clickItem(MenuEntity menuEntity);
}
