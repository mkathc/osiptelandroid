package pe.gob.osiptelandroid.presentation.expedientes.registro_trasu;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.entities.TipoDocumentoEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyRegistrarTrasu;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface RegistroTrasuContract {
    interface View extends BaseView<Presenter> {
        void getListTipoServicios(ArrayList<ServiceEntity> list);
        void getListTipoDocumentos(ArrayList<TipoDocumentoEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void sendRegistrotrasu(BodyRegistrarTrasu bodyRegistrarTrasu);
        void getTipoDocumento();
        void getServicios();
        void getRefreshToken();


      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
