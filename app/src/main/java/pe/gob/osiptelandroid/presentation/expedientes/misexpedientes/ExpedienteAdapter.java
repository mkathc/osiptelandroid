package pe.gob.osiptelandroid.presentation.expedientes.misexpedientes;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.ConsultaExpedienteEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaItem;
import pe.gob.osiptelandroid.presentation.expedientes.dialogs.ExpedientesInterface;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class ExpedienteAdapter extends LoaderAdapter<ConsultaExpedienteEntity> implements OnClickListListener {


    private Context context;
    private EncuestaItem encuestaItem;
    private SessionManager mSessionManager;
    private String cadena;
    private boolean init = true;
    private int row_index;
    ExpedientesInterface expedientesInterface;

    public ExpedienteAdapter(ArrayList<ConsultaExpedienteEntity> preguntaEntities, Context context, ExpedientesInterface expedientesInterface ) {
        super(context);
        setItems(preguntaEntities);
        this.context = context;
        this.expedientesInterface = expedientesInterface;
        mSessionManager = new SessionManager(context);
    }

    public ArrayList<ConsultaExpedienteEntity> getItems() {
        return (ArrayList<ConsultaExpedienteEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_records, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ConsultaExpedienteEntity consultaExpedienteEntity = getItems().get(position);
        ((ViewHolder) holder).tvTitle.setText("Exp. N° "+ String.valueOf(consultaExpedienteEntity.getIdExpediente()));
        ((ViewHolder) holder).tvTitleToolbar.setText("Exp. N° "+ String.valueOf(consultaExpedienteEntity.getIdExpediente()));
        ((ViewHolder) holder).tvReclamo.setText(getReclamo(consultaExpedienteEntity.getNroReclamo()));
        ((ViewHolder) holder).tvEstado.setText(consultaExpedienteEntity.getEstado());
        ((ViewHolder) holder).tvEmpresaOperadora.setText(consultaExpedienteEntity.getEmpresa());
        ((ViewHolder) holder).rlToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).rlToolbar.setVisibility(View.GONE);
                ((ViewHolder) holder).rlToolbarList.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).content.setVisibility(View.VISIBLE);
                row_index = position;
                notifyDataSetChanged();

            }
        });
        ((ViewHolder) holder).rlToolbarList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).rlToolbar.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).rlToolbarList.setVisibility(View.GONE);
                ((ViewHolder) holder).content.setVisibility(View.GONE);
            }
        });

        if (row_index == position) {
            ((ViewHolder) holder).rlToolbar.setVisibility(View.GONE);
            ((ViewHolder) holder).rlToolbarList.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).content.setVisibility(View.VISIBLE);

        } else {
            ((ViewHolder) holder).rlToolbar.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).rlToolbarList.setVisibility(View.GONE);
            ((ViewHolder) holder).content.setVisibility(View.GONE);
        }

        ((ViewHolder) holder).lyDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expedientesInterface.getExpedientePDF(consultaExpedienteEntity.getIdExpediente()+"-"+consultaExpedienteEntity.getRecurso());
            }
        });


    }

    @Override
    public void onClick(int position) {
    }

    public String  getReclamo (String nroReclamo){
        String replace = nroReclamo.replace(" ", "");
        String part1 = "";
        String[] parts = replace.split("/");
        for (int i = 0; i <parts.length ; i++) {
            if(i==0){
                part1 = parts[i];
            }else{
                part1 = parts[i] +"\n" + part1;
            }
        }

        return part1;
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnClickListListener onClickListListener;

        @BindView(R.id.tv_title_toolbar)
        TextView tvTitleToolbar;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.rl_toolbar)
        RelativeLayout rlToolbar;
        @BindView(R.id.rl_toolbar_list)
        RelativeLayout rlToolbarList;
        @BindView(R.id.tv_estado)
        TextView tvEstado;
        @BindView(R.id.tv_reclamo)
        TextView tvReclamo;
        @BindView(R.id.tv_empresa_operadora)
        TextView tvEmpresaOperadora;
        @BindView(R.id.et_num_cel)
        TextView etNumCel;
        @BindView(R.id.ly_descargar)
        LinearLayout lyDescargar;
        @BindView(R.id.content)
        ConstraintLayout content;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
