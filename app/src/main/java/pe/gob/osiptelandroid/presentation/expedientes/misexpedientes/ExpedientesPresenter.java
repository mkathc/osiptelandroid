package pe.gob.osiptelandroid.presentation.expedientes.misexpedientes;

import android.content.Context;
import android.os.Build;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.ConsultaExpedienteEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEntity;
import pe.gob.osiptelandroid.data.entities.ExpedientePdfEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class ExpedientesPresenter implements ExpedientesContract.Presenter {

    private ExpedientesContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public ExpedientesPresenter(ExpedientesContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getMisExpedientes(String nroDoc, String estado, String fechaIni, String fechaFin, String nombreUsuario, String numeroCel) {
        mView.setLoadingIndicator(true);

        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ArrayList<ConsultaExpedienteEntity>> orders = listRequest.getListExpedientes(
                "Bearer "+mSessionManager.getUserToken(),nroDoc,estado,fechaIni,fechaFin,
                "Android", Build.BRAND, Build.MODEL, numeroCel, mSessionManager.getMac() , nombreUsuario);
        orders.enqueue(new Callback<ArrayList<ConsultaExpedienteEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ConsultaExpedienteEntity>> call, Response<ArrayList<ConsultaExpedienteEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListMisExpedientes(response.body());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.getListMisExpedientes(response.body());
                            break;
                        case 401:
                            getRefreshToken();
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ConsultaExpedienteEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void validarLogin(String correo, String contraseña, String ip, String nombreUsuario, String numeroCel) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<UserTrasuEntity> orders = listRequest.validarLogin(
                "Bearer "+mSessionManager.getUserToken(),correo, contraseña,ip,
                "Android", Build.BRAND, Build.MODEL, numeroCel, ip , nombreUsuario);
        orders.enqueue(new Callback<UserTrasuEntity>() {
            @Override
            public void onResponse(Call<UserTrasuEntity> call, Response<UserTrasuEntity> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);

                if (response.isSuccessful()) {
                    mView.isLogin(response.body());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<UserTrasuEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);

                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getMyExpedientePDF(String nombreCompleto, String numeroDocumento, String idUsuario, String idExpediente) {
        mView.loadingPdf(true);
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ExpedientePdfEntity> orders = listRequest.getExpedientePDF(
                "Bearer "+mSessionManager.getUserToken(),idExpediente, numeroDocumento,idUsuario, nombreCompleto);
        orders.enqueue(new Callback<ExpedientePdfEntity>() {
            @Override
            public void onResponse(Call<ExpedientePdfEntity> call, Response<ExpedientePdfEntity> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.loadingPdf(false);

                if (response.isSuccessful()) {
                    mView.getMyExpediente(response.body().getUrl());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<ExpedientePdfEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.loadingPdf(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getEstados() {
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ArrayList<EstadoEntity>> orders = listRequest.getEstados(
                "Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<EstadoEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<EstadoEntity>> call, Response<ArrayList<EstadoEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    mView.getListEstados(response.body());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<EstadoEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                // mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }


    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getEstados();
                }else{
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
