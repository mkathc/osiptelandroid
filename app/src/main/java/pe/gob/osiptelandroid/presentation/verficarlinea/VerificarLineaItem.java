package pe.gob.osiptelandroid.presentation.verficarlinea;

import pe.gob.osiptelandroid.data.entities.OperadoraEntity;

/**
 * Created by katherine on 24/04/17.
 */

public interface VerificarLineaItem {

    void clickItem(OperadoraEntity verificarLineaEntity);
}
