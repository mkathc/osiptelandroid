package pe.gob.osiptelandroid.presentation.guia;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.GuiaEntity;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class GuiaAdapter extends LoaderAdapter<GuiaEntity> implements OnClickListListener {

    private Context context;
    private GuiaItem guiaItem;

    public GuiaAdapter(ArrayList<GuiaEntity> guiaEntities, Context context, GuiaItem guiaItem) {
        super(context);
        setItems(guiaEntities);
        this.context = context;
        this.guiaItem = guiaItem;

    }

    public ArrayList<GuiaEntity> getItems() {
        return (ArrayList<GuiaEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdTipoReclamo();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_guia_informativa, parent, false);
        return new ViewHolder(root, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        GuiaEntity guiaEntity = getItems().get(position);
        ((ViewHolder) holder).tvName.setText(guiaEntity.getDescripcion());
        ((ViewHolder) holder).imIcon.setImageDrawable(guiaEntity.getIcon());

    }

    @Override
    public void onClick(int position) {
        //Open Fragment
        GuiaEntity guiaEntity = getItems().get(position);
        guiaItem.clickItem(guiaEntity);
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.im_icon)
        ImageView imIcon;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.rl_menu)
        RelativeLayout rlMenu;

        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
