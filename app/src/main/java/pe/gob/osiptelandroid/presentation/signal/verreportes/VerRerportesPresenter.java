package pe.gob.osiptelandroid.presentation.signal.verreportes;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.VerReporteEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class VerRerportesPresenter implements VerReportesContract.Presenter {

    private VerReportesContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public VerRerportesPresenter(VerReportesContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getVerReportes(String departamento, String provincia, String distrito, String localidad) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<VerReporteEntity>> orders = listRequest.getListReportes("Bearer "+mSessionManager.getUserToken(), departamento, provincia, distrito, localidad);
        orders.enqueue(new Callback<ArrayList<VerReporteEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<VerReporteEntity>> call, Response<ArrayList<VerReporteEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListVerReportes(response.body());
                } else {
                    mView.getListVerReportes(null);

                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<VerReporteEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                // mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

   /* @Override
    public void getMisExpedientes(String nroDoc, String estado, String fechaIni, String fechaFin) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ArrayList<ConsultaExpedienteEntity>> orders = listRequest.getListExpedientes("Bearer "+mSessionManager.getUserToken(),nroDoc,estado,fechaIni,fechaFin);
        orders.enqueue(new Callback<ArrayList<ConsultaExpedienteEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ConsultaExpedienteEntity>> call, Response<ArrayList<ConsultaExpedienteEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListMisExpedientes(response.body());
                } else {
                    mView.getListMisExpedientes(null);

                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ConsultaExpedienteEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                // mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void validarLogin(String correo, String contraseña, String ip) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<UserTrasuEntity> orders = listRequest.validarLogin("Bearer "+mSessionManager.getUserToken(),correo, contraseña,ip);
        orders.enqueue(new Callback<UserTrasuEntity>() {
            @Override
            public void onResponse(Call<UserTrasuEntity> call, Response<UserTrasuEntity> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.isLogin(response.body());
                } else {
                    APIError apiError = ErrorUtils.trasuParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 500:
                            mView.showErrorMessage("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                    // mView.showErrorMessage("Error al realizar el registro");
                }
            }

            @Override
            public void onFailure(Call<UserTrasuEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                // mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getEstados() {

    }*/
}
