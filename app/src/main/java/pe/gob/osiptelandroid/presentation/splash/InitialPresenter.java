package pe.gob.osiptelandroid.presentation.splash;

import android.content.Context;
import android.util.Log;

import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEncuestaEntity;
import pe.gob.osiptelandroid.data.entities.TimeSessionEntity;
import pe.gob.osiptelandroid.data.entities.UserEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class InitialPresenter implements InitialContract.Presenter {

    private InitialContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public InitialPresenter(InitialContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void sendMac(final String mac) {
        mSessionManager.setMac(mac);
        LoginRequest loginRequest = ServiceFactory.createService(LoginRequest.class);
        Call<ArrayList<UserEntity>> orders = loginRequest.sendMac("Bearer " + mSessionManager.getUserToken(), mac);
        Log.e("sendMac", "send Mac log");
        orders.enqueue(new Callback<ArrayList<UserEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<UserEntity>> call, Response<ArrayList<UserEntity>> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {

                    if (response.body().size() != 0) {
                        mSessionManager.openSession(response.body().get(0));
                        // getEncuesta(mSessionManager.getUserEntity().getIdUsuario());
                    }

                    mView.getUserResponse();

                } else {
                    mView.showErrorMessage("Error al obtener las órdenes");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                try{
                    mView.sendLogError("SEND_MAC - "+t.getMessage());
                }catch (Exception ex){

                }finally {
                    mView.getNotConnectionError();
                }
            }
        });
    }

    @Override
    public void getEncuesta(int idUsuario) {
        Log.e("idUsuario", String.valueOf(idUsuario));
        LoginRequest loginRequest = ServiceFactory.createService(LoginRequest.class);
        Call<ArrayList<EstadoEncuestaEntity>> orders = loginRequest.getEstadoEncuesta("Bearer " + mSessionManager.getUserToken(), idUsuario);
        Log.e("getEncuesta", "getEncuesta");
        orders.enqueue(new Callback<ArrayList<EstadoEncuestaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<EstadoEncuestaEntity>> call, Response<ArrayList<EstadoEncuestaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    if (response.body().size() == 0) {
                        mSessionManager.setEncuestaActiva(false);
                    } else {
                        if (response.body().get(0).getIdUsuario() == null) {
                            mSessionManager.setEncuestaActiva(false);
                        } else {
                            mSessionManager.setEncuestaActiva(true);
                        }
                    }

                    mView.getMyEncuestaResponse();

                } else {

                    mView.getMyEncuestaResponseError();

                }
            }

            @Override
            public void onFailure(Call<ArrayList<EstadoEncuestaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                try{
                    mView.sendLogError("GET_QUIZ - " + t.getMessage());
                }catch (Exception ex){

                }finally {
                    mView.getNotConnectionError();
                }
            }
        });
    }

    @Override
    public void getToken(String idClient, String secretClient, String grantType) {

        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        Log.e("getToken", "getToken");

        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mView.getUserToken(response.body());

                } else {

                    mView.showErrorMessage("Error al obtener el token");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                try{
                    mView.sendLogError("GET_TOKEN - " + t.getMessage());
                }catch (Exception ex){

                }finally {
                    mView.getNotConnectionError();
                }
            }
        });
    }

    @Override
    public void getTimeSession() {
        ListRequest listRequest = ServiceFactory.createTrasuService(ListRequest.class);
        Call<ArrayList<TimeSessionEntity>> orders = listRequest.getTimeSession("Bearer " + mSessionManager.getUserToken());
        Log.e("getTimeSession", "getTimeSession");
        orders.enqueue(new Callback<ArrayList<TimeSessionEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<TimeSessionEntity>> call, Response<ArrayList<TimeSessionEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    if(response.body().size()!=0){
                        mSessionManager.setStatus(Integer.valueOf(response.body().get(0).getTiempo()));
                    }else {
                        mSessionManager.setStatus(30000);
                    }
                } else {
                    mSessionManager.setStatus(30000);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TimeSessionEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }

                try{
                    mView.sendLogError("GET_TIME_SESSION - "  + t.getMessage());
                }catch (Exception ex){

                }finally {
                    mView.getNotConnectionError();
                }

            }
        });
    }


}

