package pe.gob.osiptelandroid.presentation.oficinas;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.InfoWindowData;
import pe.gob.osiptelandroid.data.entities.OficinasEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.CustomInfoWindow;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by junior on 27/08/16.
 */
public class OficinasFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, OficinasContract.View {

    private static final String TAG = OficinasFragment.class.getSimpleName();

    GoogleMap mMap;
    @BindView(R.id.spinner_departamentos)
    Spinner spinnerDepartamentos;
    private View mapView;
    SupportMapFragment mapFragment;
    private OficinasContract.Presenter mPresenter;
    private SessionManager mSessionManager;
    private String idDepartamento;
    private boolean first = true;
    private boolean firstGps = true;
    private double latitude, longitude;
    private boolean isAll = false;
    private String ubication;
    private boolean isFirst = true;
    private boolean isFirstMapCenter = true;
    private LocationManager mlocManager;
    AlertDialog dialogogps;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    LatLng center;
    Geocoder geocoder;

    public OficinasFragment() {
        // Requires empty public constructor
    }

    public static OficinasFragment newInstance() {
        return new OficinasFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new OficinasPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.offices_fragment, container, false);
        ButterKnife.bind(this, root);
        mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS) != null) {
            getListDepartamentos((mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS)));
        } else {
            mPresenter.getDepartamentos();
        }
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fr = getFragmentManager();
            FragmentTransaction ft = fr.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
        // mPresenter.getDepartamentos();
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getContext() != null) {
                    mLastLocation = location;
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                    if (firstGps) {
                        LatLng latLng = new LatLng(latitude, longitude);
                        firstGps = false;
                        getDireccion();

                    }
                }
            }
        }
    };

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void getListOficinas(ArrayList<OficinasEntity> list) {
        mMap.clear();
        for (int i = 0; i < list.size(); i++) {
            final LatLng puntos = new LatLng(Double.valueOf(list.get(i).getLatitud()), Double.valueOf(list.get(i).getLongitud()));
            final InfoWindowData info = new InfoWindowData();
            info.setDireccion(list.get(i).getDireccion());
            if (list.get(i).getHorarioS() != null) {
                info.setHorarios("L-V: " + list.get(i).getHorarioLV() + "\nS: " + list.get(i).getHorarioS());
            } else {
                info.setHorarios("L-V: " + list.get(i).getHorarioLV());
            }

            info.setSede(list.get(i).getNombreOficina());

            CustomInfoWindow customInfoWindow = new CustomInfoWindow(getContext());
            mMap.setInfoWindowAdapter(customInfoWindow);
            Picasso.get().load(R.drawable.marker_celeste).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * 0.7), (int) (bitmap.getHeight() * 0.7), true);
                    Marker m = mMap.addMarker(new MarkerOptions().position(puntos).icon(BitmapDescriptorFactory.fromBitmap(newBitmap)));
                    m.setTag(info);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
                   /*Marker m = mMap.addMarker(new MarkerOptions().position(puntos).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                   m.setTag(info);*/

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(puntos, 8));

            // m.showInfoWindow();
        }

        if (isAll) {
            latitude = -10.640173;
            longitude = -75.516042;
            LatLng centerPoint = new LatLng(latitude, longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(centerPoint, 5));
            isAll = false;
        }
    }

    @Override
    public void getListDepartamentos(final ArrayList<DepartamentoEntity> list) {
        mSessionManager.saveArrayListDepartamentos(list);
        ArrayList<String> listDepartamentos = new ArrayList<>();
        listDepartamentos.add("Seleccione");
        listDepartamentos.add("Todos los departamentos");
        for (int i = 0; i < list.size(); i++) {
            listDepartamentos.add(list.get(i).getDepartamento());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDepartamentos);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartamentos.setAdapter(dataAdapter);

        spinnerDepartamentos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                if (first) {
                    first = false;
                } else {
                    if (!spinnerDepartamentos.getItemAtPosition(position).toString().equals("Seleccione")) {
                        if (!spinnerDepartamentos.getItemAtPosition(position).toString().equals("Todos los departamentos")) {
                            getConsulta(Integer.valueOf(getIdDepartamento(list, spinnerDepartamentos.getItemAtPosition(position).toString())),
                                    spinnerDepartamentos.getItemAtPosition(position).toString());

                        } else {
                            isAll = true;
                            getConsulta(-1, "");
                            //TODOS LOS DEPARTAMENTOS
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private String getIdDepartamento(ArrayList<DepartamentoEntity> list, String departamento) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getDepartamento().equals(departamento)) {
                idDepartamento = list.get(i).getCodigo();
            }
        }
        return idDepartamento;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(OficinasContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {

    }

    public void getDireccion() {

        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(
                    latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                ubication = address.getAdminArea();

            }
        } catch (Exception ex) {

        }

        getConsulta(0, ubication);
    }


    private void getConsulta(int idDepartamento, String ubication) {
        if (mSessionManager.getUserEntity() != null) {
            mPresenter.getOficinas(idDepartamento, ubication,
                    mSessionManager.getUserEntity().getIdUsuario()+"",
                    mSessionManager.getUserEntity().getNroTelefono());

        } else {
            mPresenter.getOficinas(idDepartamento, ubication, " ", " ");
        }
    }

    public void alertafalta() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El sistema GPS esta desactivado, para continuar presione el boton activar");
        builder.setCancelable(true);
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogogps.dismiss();
            }
        });
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent
                        (Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        dialogogps = builder.create();
        dialogogps.show();
        dialogogps.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSessionManager.setGPSstatus(false);
                setMapToCenter();
            }
        });
    }

    public void setMapToCenter() {
        if (isFirstMapCenter) {
            isFirstMapCenter = false;
            latitude = -10.640173;
            longitude = -75.516042;
            LatLng centerPoint = new LatLng(latitude, longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(centerPoint, 5));
        }

    }

    public void updateLocation() {
        if (isFirst) {
            isFirst = false;
            isFirstMapCenter = false;
            if (!mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // Call your Alert message
                if (mSessionManager.getFirtsGPSstatus()) {
                    startActivity(new Intent
                            (Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    mSessionManager.FirtsGPSstatus(false);
                    mSessionManager.setGPSstatus(false);
                } else {
                    if (mSessionManager.getGPSstatus()) {
                        alertafalta();
                    }
                }
            }
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(1000);
            mLocationRequest.setFastestInterval(1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    //locationStart();
                } else {
                    //checkLocationPermission();
                }
            }
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

        }
        mMap.setMyLocationEnabled(true);

    }
}
