package pe.gob.osiptelandroid.presentation.imei.reclamo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.ActivityUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ProblemasListFragment extends BaseFragment {

    private static final String TAG = ProblemasListFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    Unbinder unbinder;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.body)
    FrameLayout body;
    @BindView(R.id.button_next_floating)
    FloatingActionButton buttonNextFloating;

    private ProblemasListAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private SessionManager mSessionManager;
    private sendDataReportImeiInterface mCallback;
    private ArrayList<ProblemaEntity> list;

    public ProblemasListFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.button_next_floating)
    public void onViewClicked() {
        if (mSessionManager.getProblema() == null) {
            Toast.makeText(getContext(), "Por favor seleccione un problema para continuar", Toast.LENGTH_SHORT).show();
        } else {
            mCallback.sendDataToReportImei(mSessionManager.getProblema());
        }
    }


    public interface sendDataReportImeiInterface {
        void sendDataToReportImei(ProblemaEntity problemaEntity);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (sendDataReportImeiInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    public static ProblemasListFragment newInstance(Bundle bundle) {
        ProblemasListFragment fragment = new ProblemasListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        list = new ArrayList<>();
        list = (ArrayList<ProblemaEntity>) getArguments().getSerializable("problemasList");

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.list_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        tvTitle.setText("Seleccione el tipo de problema detectado");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.sendDataToReportImei(null);
            }
        });

        return root;
    }


    void remove() {
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mAdapter = new VerReportesAdapter(getList());
        if (mSessionManager.getProblema() != null) {
            Log.e("get Problema", mSessionManager.getProblema().getDescripcion());
        }
        mAdapter = new ProblemasListAdapter(new ArrayList<ProblemaEntity>(), getContext(),
                mSessionManager.getProblema());
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
        if (list != null) {
            mAdapter.setItems(list);
        }
    }

}
