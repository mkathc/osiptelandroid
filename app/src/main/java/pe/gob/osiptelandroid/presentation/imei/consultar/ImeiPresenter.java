package pe.gob.osiptelandroid.presentation.imei.consultar;

import android.content.Context;
import android.os.Build;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.ConsultaImeiEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class ImeiPresenter implements ImeiContract.Presenter {

    private ImeiContract.View mView;
    private Context context;
    private SessionManager mSessionManager;
    private String imei, nombreUsuario, numeroCel;

    public ImeiPresenter(ImeiContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void sendImei(String imei, String nombreUsuario, String numeroCel) {
        this.imei = imei;
        this.nombreUsuario = nombreUsuario;
        this.numeroCel = numeroCel;
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createSigemService(ListRequest.class);
        Call<ArrayList<ConsultaImeiEntity>> orders = listRequest.getConsultaImei(
                "Bearer "+mSessionManager.getUserToken(),imei, mSessionManager.getMac(),
                "Android", Build.BRAND, Build.MODEL, numeroCel,mSessionManager.getMac(), nombreUsuario);
        orders.enqueue(new Callback<ArrayList<ConsultaImeiEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ConsultaImeiEntity>> call, Response<ArrayList<ConsultaImeiEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        mView.imeiSuccess(response.body());
                    }else{
                        APIError apiError = ErrorUtils.sigemParseError(response);
                        switch (response.code()){
                            case 400:
                                mView.imeiFailure(apiError.getMessages().get(0));
                                break;
                            case 401:
                                getRefreshToken();
                                break;
                            case 500:
                                mView.imeiFailure("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                                break;
                            default:
                                mView.imeiFailure("Error desconocido");
                                break;
                        }
                      //  mView.showErrorMessage("Error al obtener la consulta de su imei");
                    }

                } else {
                    APIError apiError = ErrorUtils.sigemParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.imeiFailure(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.imeiFailure("Ocurrió un error al conectar al servidor, intente nuevamente por favor");
                            break;
                        default:
                            mView.imeiFailure("Error desconocido");
                            break;
                    }

                   // mView.showErrorMessage("Error al obtener la consulta de su imei");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ConsultaImeiEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.imeiFailure("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    sendImei(imei,nombreUsuario, numeroCel);
                }else{
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
