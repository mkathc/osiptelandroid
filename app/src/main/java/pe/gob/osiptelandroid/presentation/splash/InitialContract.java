package pe.gob.osiptelandroid.presentation.splash;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;

/**
 * Created by katherine on 12/05/17.
 */

public interface InitialContract {
    interface View extends BaseView<Presenter> {

        void getUserResponse();
        void getUserToken(AccessTokenEntity accessTokenEntity);
        void getMyEncuestaResponse();
        void getMyEncuestaResponseError();
        void getNotConnectionError();
        void sendLogError(String error);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void sendMac(String mac);

        void getEncuesta(int idUsuario);

        void getToken(String idClient, String secretClient, String grantType);

        void getTimeSession();


      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
