package pe.gob.osiptelandroid.presentation.verficarlinea;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.CustomTabActivityHelper;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.WebviewFallback;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class VerificarLineaFragment extends BaseFragment implements VerificarLineaItem, VerificarContract.View {

    private static final String TAG = VerificarLineaFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bartitle)
    RelativeLayout bartitle;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    Unbinder unbinder;


    private VerificarLineaAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private VerificarContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private SessionManager mSessionManager;
    private activateMenu mCallback;

    public VerificarLineaFragment() {
        // Requires empty public constructor
    }

    public static VerificarLineaFragment newInstance() {
        return new VerificarLineaFragment();
    }

    public interface activateMenu{
        void sendActivateMenu(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (VerificarLineaFragment.activateMenu) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new VerificarPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.verificar_linea_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                remove();
            }
        });
        return root;
    }

    private void remove() {
        mCallback.sendActivateMenu(true);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        mAdapter = new VerificarLineaAdapter(new ArrayList<OperadoraEntity>(), getContext(), (VerificarLineaItem) this);
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
        bartitle.setVisibility(View.GONE);
        mPresenter.getOperadores();
    }


    @Override
    public void clickItem(OperadoraEntity verificarLineaItem) {
       // Bundle bundle = new Bundle();
      //  bundle.putString("url", verificarLineaItem.getEnlace());
       // nextActivity(getActivity(),bundle, WebViewActivity.class, false);
        final Bitmap backButton = BitmapFactory.decodeResource(getResources(), R.drawable.ic_keyboard_arrow_left_24dp);
        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                .setToolbarColor(getActivity().getResources().getColor(R.color.colorPrimary))
                .setCloseButtonIcon(backButton)
                .setStartAnimations(getContext(), android.R.anim.fade_in, android.R.anim.fade_out)
                .setExitAnimations(getContext(), android.R.anim.fade_in, android.R.anim.fade_out)
                .build();
        CustomTabActivityHelper.openCustomTab(
                getActivity(), customTabsIntent, Uri.parse(getUrl(verificarLineaItem.getEnlace())), new WebviewFallback());

    }

    private String getUrl(String url){

        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        return url;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getListOperadores(ArrayList<OperadoraEntity> list) {
        if (list != null) {
            mSessionManager.saveArrayListOperadores(list);
            bartitle.setVisibility(View.VISIBLE);
            mAdapter.setItems(list);
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(VerificarContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {

    }
}
