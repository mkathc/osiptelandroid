package pe.gob.osiptelandroid.presentation.imei.dialogs;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleNoEncontradoImei extends AlertDialog {
    @BindView(R.id.im_close)
    ImageView imClose;
    @BindView(R.id.tv_imei)
    TextView tvImei;
    @BindView(R.id.tv_third)
    TextView tvThird;

    public DetalleNoEncontradoImei(Context context, String imei, String message) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_detalle_no_encontrado_imei, null);
        setView(view);
        ButterKnife.bind(this, view);
        tvImei.setText(imei);
        tvThird.setText(message);
    }

    @OnClick(R.id.im_close)
    public void onViewClicked() {
        dismiss();
    }
}
