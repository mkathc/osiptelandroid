package pe.gob.osiptelandroid.presentation.signal.reportar;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.NroEncuesta;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaCoberturaEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.UbicationLatLongEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyReporteCobertura;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.data.remote.request.PostRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class ReportarSignalPresenter implements ReportarSignalContract.Presenter {

    private ReportarSignalContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public ReportarSignalPresenter(ReportarSignalContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getOperadores() {
        //  mView.showMessage(imei);
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<OperadoraEntity>> orders = listRequest.getOperadores("Bearer " + mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<OperadoraEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<OperadoraEntity>> call, Response<ArrayList<OperadoraEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListOperadores(response.body());

                } else {
                    mView.setLoadingIndicator(false);
                    APIError apiError = ErrorUtils.firstParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de operadores, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<OperadoraEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getProblemasCobertura() {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<ProblemaCoberturaEntity>> orders = listRequest.getProblemasCobertura("Bearer " + mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<ProblemaCoberturaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ProblemaCoberturaEntity>> call, Response<ArrayList<ProblemaCoberturaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        mView.getListProblemasCobertura(response.body());
                    } else {
                        APIError apiError = ErrorUtils.coberturaParserError(response);
                        switch (response.code()) {
                            case 400:
                                mView.showErrorMessage(apiError.getMessages().get(0));
                                break;
                            case 401:
                                getRefreshToken();
                                break;
                            case 500:
                                mView.showErrorMessage("Error al obtener la lista de problemas, intente nueamente por favor");
                                break;
                            default:
                                mView.showErrorMessage("Error desconocido");
                                break;
                        }
                    }

                } else {
                    mView.showErrorMessage("Se presentó un error al registrar su reporte, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ProblemaCoberturaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendReport(BodyReporteCobertura bodyReporteCobertura) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createCoberturaService(PostRequest.class);
        Call<NroEncuesta> orders = postRequest.sendReporteCobertura(
                "Bearer " + mSessionManager.getUserToken(), bodyReporteCobertura);
        orders.enqueue(new Callback<NroEncuesta>() {
            @Override
            public void onResponse(Call<NroEncuesta> call, Response<NroEncuesta> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        mView.showMessage("Su reporte " +response.body().getNumeroEncuesta()+ " se ha generado con éxito");
                    } else {
                        APIError apiError = ErrorUtils.trasuParseError(response);
                        switch (response.code()) {
                            case 400:
                                mView.showErrorMessage(apiError.getMessages().get(0));
                                break;
                            case 500:
                                mView.showErrorMessage("Ocurrió un error con el servidor, intente nuevamente por favor");
                                break;
                            default:
                                mView.showErrorMessage("Error desconocido");
                                break;
                        }
                    }

                } else {
                    mView.showErrorMessage("Se presentó un error al registrar su reporte, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<NroEncuesta> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }


    @Override
    public void getProvincia(final String codDepartamento) {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<ProvinciaEntity>> orders = listRequest.getProvincias("Bearer " + mSessionManager.getUserToken(), codDepartamento);
        orders.enqueue(new Callback<ArrayList<ProvinciaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ProvinciaEntity>> call, Response<ArrayList<ProvinciaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListProvincia(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de provincias, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ProvinciaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getDistrito(final String codDepartamento, final String codProvincia) {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<DistritoEntity>> orders = listRequest.getDistrito("Bearer " + mSessionManager.getUserToken(), codDepartamento, codProvincia);
        orders.enqueue(new Callback<ArrayList<DistritoEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<DistritoEntity>> call, Response<ArrayList<DistritoEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListDistrito(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de distritos, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DistritoEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getLocalidades(final String codDepartamento, final String codProvincia, final String codDistrito) {
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<LocalidadEntity>> orders = listRequest.getLocalidades("Bearer " + mSessionManager.getUserToken(), codDepartamento, codProvincia, codDistrito);
        orders.enqueue(new Callback<ArrayList<LocalidadEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<LocalidadEntity>> call, Response<ArrayList<LocalidadEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListLocalidades(response.body());
                    }
                } else {
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de localidades, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<LocalidadEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }

                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getListServicios(int idOperador) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<ServicioOperadorEntity>> orders = listRequest.getLisServiciosByOperador("Bearer " + mSessionManager.getUserToken(), idOperador);
        orders.enqueue(new Callback<ArrayList<ServicioOperadorEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ServicioOperadorEntity>> call, Response<ArrayList<ServicioOperadorEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    mView.getListServiciosOperador(response.body());
                } else {
                    mView.setLoadingIndicator(false);
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de servicios, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ServicioOperadorEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }

                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getListServiciosSinonimo(int idOperador) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<ServicioOperadorEntity>> orders = listRequest.getLisServiciosByOperadorEqual("Bearer " + mSessionManager.getUserToken(), idOperador);
        orders.enqueue(new Callback<ArrayList<ServicioOperadorEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ServicioOperadorEntity>> call, Response<ArrayList<ServicioOperadorEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    mView.getListServiciosOperador(response.body());
                } else {
                    mView.setLoadingIndicator(false);
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de servicios, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ServicioOperadorEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }

                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getLatLong(String codDepartamento, String codProvincia, String codDistrito, String codLocl) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createCoberturaService(ListRequest.class);
        Call<ArrayList<UbicationLatLongEntity>> orders = listRequest.getListLatLong("Bearer " + mSessionManager.getUserToken(), codDepartamento, codProvincia, codDistrito, codLocl);
        orders.enqueue(new Callback<ArrayList<UbicationLatLongEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<UbicationLatLongEntity>> call, Response<ArrayList<UbicationLatLongEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    if (response.body().size() != 0) {
                        mView.getLangLong(response.body());
                    }
                } else {
                    mView.setLoadingIndicator(false);
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()) {
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de localidades, intente nueamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UbicationLatLongEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }

                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getOperadores();
                    getProblemasCobertura();
                } else {
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
