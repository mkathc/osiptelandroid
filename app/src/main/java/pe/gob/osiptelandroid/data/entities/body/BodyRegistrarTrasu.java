package pe.gob.osiptelandroid.data.entities.body;

public class BodyRegistrarTrasu {
    private String tipoDocumento;
    private String numeroDocumento;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String correoElectronico;
    private String confirmarCorreoElectronico;
    private int idCodigoAsociado;
    private String  codigoReclamo;
    private String contrasenia;
    private String confirmarContrasenia;
    private int idServicioReclamado;
    private String sistemaOperativo;
    private String marcaCelular;
    private String modeloCelular;
    private String númeroCelular;
    private String mac;
    private String nombreUsuario;

    public int getIdCodigoAsociado() {
        return idCodigoAsociado;
    }

    public void setIdCodigoAsociado(int idCodigoAsociado) {
        this.idCodigoAsociado = idCodigoAsociado;
    }

    public int getIdServicioReclamado() {
        return idServicioReclamado;
    }

    public void setIdServicioReclamado(int idServicioReclamado) {
        this.idServicioReclamado = idServicioReclamado;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public String getMarcaCelular() {
        return marcaCelular;
    }

    public void setMarcaCelular(String marcaCelular) {
        this.marcaCelular = marcaCelular;
    }

    public String getModeloCelular() {
        return modeloCelular;
    }

    public void setModeloCelular(String modeloCelular) {
        this.modeloCelular = modeloCelular;
    }

    public String getNúmeroCelular() {
        return númeroCelular;
    }

    public void setNúmeroCelular(String númeroCelular) {
        this.númeroCelular = númeroCelular;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getConfirmarCorreoElectronico() {
        return confirmarCorreoElectronico;
    }

    public void setConfirmarCorreoElectronico(String confirmarCorreoElectronico) {
        this.confirmarCorreoElectronico = confirmarCorreoElectronico;
    }

    public String getCodigoReclamo() {
        return codigoReclamo;
    }

    public void setCodigoReclamo(String codigoReclamo) {
        this.codigoReclamo = codigoReclamo;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getConfirmarContrasenia() {
        return confirmarContrasenia;
    }

    public void setConfirmarContrasenia(String confirmarContrasenia) {
        this.confirmarContrasenia = confirmarContrasenia;
    }
}
