package pe.gob.osiptelandroid.data.entities.body;

import pe.gob.osiptelandroid.data.entities.RespuestaEntity;

import java.util.ArrayList;

public class BodyEncuestaEntity {
    private int idUsuario;
    private String usuarioRegistro;
    private ArrayList<RespuestaEntity> usuarioPregunta;
    private String sistemaOperativo;
    private String marcaCelular;
    private String modeloCelular;
    private String numeroCelular;
    private String mac;
    private String nombreUsuario;

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public String getMarcaCelular() {
        return marcaCelular;
    }

    public void setMarcaCelular(String marcaCelular) {
        this.marcaCelular = marcaCelular;
    }

    public String getModeloCelular() {
        return modeloCelular;
    }

    public void setModeloCelular(String modeloCelular) {
        this.modeloCelular = modeloCelular;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public ArrayList<RespuestaEntity> getUsuarioPregunta() {
        return usuarioPregunta;
    }

    public void setUsuarioPregunta(ArrayList<RespuestaEntity> usuarioPregunta) {
        this.usuarioPregunta = usuarioPregunta;
    }
}
