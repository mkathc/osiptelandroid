package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class DepartamentoEntity implements Serializable {
    private String codigo;
    private String departamento;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
}
