package pe.gob.osiptelandroid.data.remote.request;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEncuestaEntity;
import pe.gob.osiptelandroid.data.entities.UserEntity;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by katherine on 10/05/17.
 */

public interface LoginRequest {

    @POST("connect/token")
    Call<AccessTokenEntity> login(@Body RequestBody requestBody);

    @GET("Usuario/GetUserDataByMac")
    Call<ArrayList<UserEntity>> sendMac(@Header("Authorization") String token, @Query("mac") String mac);

    @GET("Encuesta/GetCalificanosByTemporada")
    Call<ArrayList<EstadoEncuestaEntity>> getEstadoEncuesta(@Header("Authorization") String token,
                                                            @Query("IdUsuario") int idUsuario);

}
