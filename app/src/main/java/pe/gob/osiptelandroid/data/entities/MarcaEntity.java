package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class MarcaEntity  implements Serializable {
    private int idMarca;
    private String descripcion;

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
