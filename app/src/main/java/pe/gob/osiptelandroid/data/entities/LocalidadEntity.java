package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class LocalidadEntity implements Serializable {

    private String codigo;
    private String localidad;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
}
