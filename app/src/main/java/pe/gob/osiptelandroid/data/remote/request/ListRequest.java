package pe.gob.osiptelandroid.data.remote.request;

import pe.gob.osiptelandroid.data.entities.CoberturaByOperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ConsultaExpedienteEntity;
import pe.gob.osiptelandroid.data.entities.ConsultaImeiEntity;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.EnlaceAppMovilEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEntity;
import pe.gob.osiptelandroid.data.entities.ExpedientePdfEntity;
import pe.gob.osiptelandroid.data.entities.FormatoReclamoEntity;
import pe.gob.osiptelandroid.data.entities.GuiaEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.MarcaEntity;
import pe.gob.osiptelandroid.data.entities.ModeloEntity;
import pe.gob.osiptelandroid.data.entities.OficinasEntity;
import pe.gob.osiptelandroid.data.entities.PreguntaEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaCoberturaEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.TimeSessionEntity;
import pe.gob.osiptelandroid.data.entities.TipoDocumentoEntity;
import pe.gob.osiptelandroid.data.entities.UbicationLatLongEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;
import pe.gob.osiptelandroid.data.entities.VerReporteEntity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;


/**
 * Created by katherine on 12/06/17.
 */

public interface ListRequest {

    @GET("Pregunta/GetAllPreguntas")
    Call<ArrayList<PreguntaEntity>> getPreguntas(@Header("Authorization") String token);

    @GET("Operador/GetApplicationGlobal")
    Call<EnlaceAppMovilEntity> getEnlacesAppmovil(@Header("Authorization") String token,
                                                             @Query("Aplicacion")String aplicacion,
                                                             @Query("Global") String global);

    @GET("Sigem/GetDatosEquipoByImei")
    Call<ArrayList<ConsultaImeiEntity>> getConsultaImei(@Header("Authorization") String token,
                                                        @Query("Imei") String imei,
                                                        @Query("Ip") String ip,
                                                        @Query("SistemaOperativo") String so,
                                                        @Query("MarcaCelular") String marca,
                                                        @Query("ModeloCelular") String modelo,
                                                        @Query("NúmeroCelular") String numeroCel,
                                                        @Query("MAC") String mac,
                                                        @Query("NombreUsuario") String nombreUsuario);

    @GET("Operador/GetAllOperadores")
    Call<ArrayList<OperadoraEntity>> getOperadores(@Header("Authorization") String token);

    @GET("Reclamo/GetAllPlantillaReclamo")
    Call<ArrayList<FormatoReclamoEntity>> getFormatoReclamo(@Header("Authorization") String token);

    @GET("Reclamo/GetAllTipoReclamo")
    Call<ArrayList<GuiaEntity>> getItemsList(@Header("Authorization") String token);

    @GET("Problema/GetAllMarcas")
    Call<ArrayList<MarcaEntity>> getMarcas(@Header("Authorization") String token);

    @GET("Problema/GetModelosByMarca")
    Call<ArrayList<ModeloEntity>> getModelobyMarca(@Header("Authorization") String token, @Query("idMarca") int idMarca);

    @GET("Problema/GetAllProblemasDetectados")
    Call<ArrayList<ProblemaEntity>> getProblemas(@Header("Authorization") String token);

    @GET("CoberturaMovil/GetAllDepartamentos")
    Call<ArrayList<DepartamentoEntity>> getDepartamentos(@Header("Authorization") String token);

    @GET("CoberturaMovil/GetProvinciasByDepartamento")
    Call<ArrayList<ProvinciaEntity>> getProvincias(@Header("Authorization") String token, @Query("codDepartamento") String codDep);

    @GET("CoberturaMovil/GetDistritosByDepartamentoAndProvincia")
    Call<ArrayList<DistritoEntity>> getDistrito(@Header("Authorization") String token, @Query("codDepartamento") String codDep, @Query("codProvincia") String codProv);

    @GET("CoberturaMovil/GetLocalidadesByDepartamentoProvinciaAndDistrito")
    Call<ArrayList<LocalidadEntity>> getLocalidades(@Header("Authorization") String token, @Query("codDepartamento") String codDep, @Query("codProvincia") String codProv, @Query("codDistrito") String codDistr);

    @GET("CoberturaMovil/GetOficinasOsiptel")
    Call<ArrayList<OficinasEntity>> getOficinas(@Header("Authorization") String token,
                                                @Query("IdDepartamento") int idDepartamento,
                                                @Query("NombreDepartamento") String nombreDepartamento,
                                                @Query("SistemaOperativo") String so,
                                                @Query("MarcaCelular") String marca,
                                                @Query("ModeloCelular") String modelo,
                                                @Query("NúmeroCelular") String numeroCel,
                                                @Query("MAC") String mac,
                                                @Query("NombreUsuario") String nombreUsuario);

    @GET("CoberturaMovil/GetTiposProblemas")
    Call<ArrayList<ProblemaCoberturaEntity>> getProblemasCobertura(@Header("Authorization") String token);

    @GET("CoberturaMovil/GetServiciosByOperador")
    Call<ArrayList<ServicioOperadorEntity>> getLisServiciosByOperador(
            @Header("Authorization") String token,
            @Query("IdEmpresaOperadora") int idOperador );

    @GET("CoberturaMovil/GetServiciosByOperadorEqual")
    Call<ArrayList<ServicioOperadorEntity>> getLisServiciosByOperadorEqual(
            @Header("Authorization") String token,
            @Query("IdEmpresaOperadora") int idOperador );

    @GET("CoberturaMovil/GetUbicacionActual")
    Call<ArrayList<UbicationLatLongEntity>> getListLatLong(@Header("Authorization") String token,
                                                           @Query("Departamento") String departamento,
                                                           @Query("Provincia") String provincia,
                                                           @Query("Distrito") String distrito,
                                                           @Query("Localidad") String localidad);

    @GET("CoberturaMovil/GetCoberturaByOperadora")
    Call<ArrayList<CoberturaByOperadoraEntity>> getConsultaCobertura(
            @Header("Authorization") String token,
            @Query("Departamento") String departamento,
            @Query("Provincia") String provincia,
            @Query("Distrito") String distrito,
            @Query("Localidad") String localidad,
            @Query("Operador") String idOperadora,
            @Query("IP") String ip,
            @Query("SistemaOperativo") String so,
            @Query("MarcaCelular") String marca,
            @Query("ModeloCelular") String modelo,
            @Query("NúmeroCelular") String numeroCel,
            @Query("MAC") String mac,
            @Query("NombreUsuario") String nombreUsuario);

    @GET("CoberturaMovil/GetListReporteCobertura")
    Call<ArrayList<VerReporteEntity>> getListReportes(@Header("Authorization") String token,
                                                      @Query("Departamento") String departamento,
                                                      @Query("Provincia") String provincia,
                                                      @Query("Distrito") String distrito,
                                                      @Query("Localidad") String localidad);

    @GET("Trasu/ValidarLogin")
    Call<UserTrasuEntity> validarLogin(@Header("Authorization") String token,
                                       @Query("Correo") String correo,
                                       @Query("Clave") String clave,
                                       @Query("Ip") String ip,
                                       @Query("SistemaOperativo") String so,
                                       @Query("MarcaCelular") String marca,
                                       @Query("ModeloCelular") String modelo,
                                       @Query("NumeroCelular") String numeroCel,
                                       @Query("MAC") String mac,
                                       @Query("NombreUsuario") String nombreUsuario);


    @GET("Trasu/GetConsultarExpediente")
    Call<ArrayList<ConsultaExpedienteEntity>> getListExpedientes(@Header("Authorization") String token,
                                                                 @Query("NumeroDocumento") String nroDoc,
                                                                 @Query("Estado") String estado,
                                                                 @Query("FechaInicio") String fechaInic,
                                                                 @Query("FechaFin") String fechaFin,
                                                                 @Query("SistemaOperativo") String so,
                                                                 @Query("MarcaCelular") String marca,
                                                                 @Query("ModeloCelular") String modelo,
                                                                 @Query("NúmeroCelular") String numeroCel,
                                                                 @Query("MAC") String mac,
                                                                 @Query("NombreUsuario") String nombreUsuario);

    @GET("Trasu/ListarTipoDocumento")
    Call<ArrayList<TipoDocumentoEntity>> getTipoDocumento(@Header("Authorization") String token);

    @GET("Trasu/ListarEstados")
    Call<ArrayList<EstadoEntity>> getEstados(@Header("Authorization") String token);


    @GET("Trasu/ListarServicio")
    Call<ArrayList<ServiceEntity>> getListServicios(@Header("Authorization") String token);

    @GET("Trasu/GetDescargarExpediente")
    Call<ExpedientePdfEntity> getExpedientePDF(@Header("Authorization") String token,
                                               @Query("IdExpediente") String idExpediente,
                                               @Query("NumeroDocumento") String numeroDocumento,
                                               @Query("IdUsuario") String idUsuario,
                                               @Query("NombreCompleto") String nombreCompleto);

    @GET("Trasu/GetTiempoSessionExpediente")
    Call<ArrayList<TimeSessionEntity>> getTimeSession(@Header("Authorization") String token);
}
