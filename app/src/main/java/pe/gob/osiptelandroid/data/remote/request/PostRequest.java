package pe.gob.osiptelandroid.data.remote.request;

import pe.gob.osiptelandroid.data.entities.NroEncuesta;
import pe.gob.osiptelandroid.data.entities.UserEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.entities.body.BodyRegistrarTrasu;
import pe.gob.osiptelandroid.data.entities.body.BodyReportarImei;
import pe.gob.osiptelandroid.data.entities.body.BodyEncuestaEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyReporteCobertura;
import pe.gob.osiptelandroid.data.entities.body.BodyUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by kath on 25/01/18.
 */

public interface PostRequest {
    @POST("Usuario/CreateCountUser")
    Call<ArrayList<UserEntity>> sendRegister(@Header("Authorization") String token,
                                             @Body BodyUser bodyUser);

    @POST("Usuario/CreateUsuarioPregunta")
    Call<Void> sendEncuesta(@Header("Authorization")String token,@Body BodyEncuestaEntity encuestaEntity);

    @POST("Sigem/CreateReportarProblemaImei")
    Call<Void> sendReporteImei(@Header("Authorization")String token,@Body BodyReportarImei reportarImei);

    @POST("CoberturaMovil/CreateReporteCobertura")
    Call<NroEncuesta> sendReporteCobertura(@Header("Authorization")String token,
                                           @Body BodyReporteCobertura bodyReporteCobertura);

    @POST("Trasu/CreateRegistroCuentaAcceso")
    Call<Void> sendRegitroTrasu(@Header("Authorization")String token, @Body BodyRegistrarTrasu bodyRegistrarCobertura);

    @POST("EventoLog/CreateEventoLog")
    Call<Void> sendLog(@Header("Authorization")String token, @Body BodyEntityLog bodyEntityLog);

}
