package pe.gob.osiptelandroid.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.entities.RespuestaEntity;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.UserEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by junior on 13/10/16.
 */
public class SessionManager {


    public static final String PREFERENCE_NAME = "OsiptelClient";
    public static int PRIVATE_MODE = 0;

    /**
     * USUARIO DATA SESSION - JSON
     */
    public static final String USER_MAC = "user_mac";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_JSON = "user_json";
    public static final String USER_COMMENT = "user_comment";
    public static final String IS_LOGIN = "user_login";
    public static final String ENCUESTA_ACTIVA = "encuesta_activa";
    public static final String ID_COMENTARIO = "id_comentario";
    public static final String ID_CALIFICANOS = "id_calificanos";
    public static final String RATING = "rating";
    public static final String ARRAY_ENCUESTA = "array_encuesta";
    public static final String ARRAY_DEPARTAMENTOS = "array_departamentos";
    public static final String ARRAY_OPERADORES = "array_operadores";
    public static final String ARRAY_SEND_OPERADORES = "array_send_operadores";
    public static final String ARRAY_LISTA_SERVICIOS = "array_lista_servicios";
    public static final String STATUS_GPS = "status_gps";
    public static final String FIRST_STATUS_GPS = "first_status_gps";
    public static final String STATUS_COUNT = "status_count";
    public static final String LOGIN_TRASU = "login_trasu";
    public static final String USER_TRASU = "user_trasu";
    public static final String PROBLEMA = "problema";
    public static final String SERVICE_TRASU = "service_trasu";
    public static final String ENLACE_COMPARATEL = "enlace_comparatel";
    public static final String ENLACE_COMPARAMOVIL = "enlace_comparamovil";
    public static final String ENLACE_PUNKU = "enlace_punku";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public boolean isLogin() {
        return preferences.getBoolean(IS_LOGIN, false);
    }


    public void openSession(UserEntity userEntity) {
        editor.putBoolean(IS_LOGIN, true);
        if (userEntity != null) {
            Gson gson = new Gson();
            String user = gson.toJson(userEntity);
            editor.putString(USER_JSON, user);
        }
        editor.commit();
    }

    public UserEntity getUserEntity() {
        String userData = preferences.getString(USER_JSON, null);
        return new Gson().fromJson(userData, UserEntity.class);
    }


    public boolean isEncuestaActiva() {
        return preferences.getBoolean(ENCUESTA_ACTIVA, false);
    }

    public void setEncuestaActiva(boolean active) {
        editor.putBoolean(ENCUESTA_ACTIVA, active);
        editor.commit();
    }

    public String getUserToken() {
        return preferences.getString(USER_TOKEN, null);
    }

    public void setUserToken(String token) {
        editor.putString(USER_TOKEN, token);
        editor.commit();
    }


    public String getMac() {
        return preferences.getString(USER_MAC, null);
    }

    public void setMac(String mac) {
        editor.putString(USER_MAC, mac);
        editor.commit();
    }

    public void saveArrayList(ArrayList<RespuestaEntity> list) {
        ;
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(ARRAY_ENCUESTA, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public ArrayList<RespuestaEntity> getArrayList(String key) {
        Gson gson = new Gson();
        String json = preferences.getString(key, null);
        Type type = new TypeToken<ArrayList<RespuestaEntity>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public String getComentario() {
        return preferences.getString(USER_COMMENT, null);
    }

    public void setComentario(String comentario) {
        editor.putString(USER_COMMENT, comentario);
        editor.commit();
    }

    public int getIdComentario() {
        return preferences.getInt(ID_COMENTARIO, 5);
    }

    public void setIdComentario(int idComentario) {
        editor.putInt(ID_COMENTARIO, idComentario);
        editor.commit();
    }

    public int getIdCalificanos() {
        return preferences.getInt(ID_CALIFICANOS, 6);
    }

    public void setIdCalificanos(int idCalificanos) {
        editor.putInt(ID_CALIFICANOS, idCalificanos);
        editor.commit();
    }

    public String getRating() {
        return preferences.getString(RATING, "5");
    }

    public void setRating(String rating) {
        editor.putString(RATING, rating);
        editor.commit();
    }

    public void saveArrayListDepartamentos(ArrayList<DepartamentoEntity> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(ARRAY_DEPARTAMENTOS, json);
        editor.apply();
    }

    public ArrayList<DepartamentoEntity> getArrayListDepartamentos(String key) {
        Gson gson = new Gson();
        String json = preferences.getString(key, null);
        Type type = new TypeToken<ArrayList<DepartamentoEntity>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public void saveArrayListOperadores(ArrayList<OperadoraEntity> list) {
        ;
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(ARRAY_OPERADORES, json);
        editor.apply();
    }

    public ArrayList<OperadoraEntity> getArrayListOperadores(String key) {
        Gson gson = new Gson();
        String json = preferences.getString(key, null);
        Type type = new TypeToken<ArrayList<OperadoraEntity>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveArrayListSendOperadoresCobertura(ArrayList<String> list) {
        ;
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(ARRAY_SEND_OPERADORES, json);
        editor.apply();
    }

    public ArrayList<String> getArrayListSendOperadoresCobertura(String key) {
        Gson gson = new Gson();
        String json = preferences.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveArrayListaServiciosOperadores(ArrayList<ServicioOperadorEntity> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(ARRAY_LISTA_SERVICIOS, json);
        editor.apply();
    }

    public ArrayList<ServicioOperadorEntity> getArrayListaServiciosOperadores(String key) {
        Gson gson = new Gson();
        String json = preferences.getString(key, null);
        Type type = new TypeToken<ArrayList<ServicioOperadorEntity>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public boolean getGPSstatus() {
        return preferences.getBoolean(STATUS_GPS, false);
    }

    public void setGPSstatus(boolean status) {
        editor.putBoolean(STATUS_GPS, status);
        editor.commit();
    }

    public boolean getFirtsGPSstatus() {
        return preferences.getBoolean(FIRST_STATUS_GPS, true);
    }

    public void FirtsGPSstatus(boolean status) {
        editor.putBoolean(FIRST_STATUS_GPS, status);
        editor.commit();
    }

    public void setStatus(int status) {
        editor.putInt(STATUS_COUNT, status);
        editor.commit();
    }

    public int getStatus() {
        return preferences.getInt(STATUS_COUNT, 30000);
        //return preferences.getInt(STATUS_COUNT, 182000);
    }

    public boolean getLoginTrasu() {
        return preferences.getBoolean(LOGIN_TRASU, false);
    }

    public void setLoginTrasu(boolean status) {
        editor.putBoolean(LOGIN_TRASU, status);
        editor.commit();
    }

    public UserTrasuEntity getUserTrasu() {
        String userData = preferences.getString(USER_TRASU, null);
        return new Gson().fromJson(userData, UserTrasuEntity.class);
    }

    public void setUserTrasu(UserTrasuEntity userTrasu) {
        if (userTrasu != null) {
            Gson gson = new Gson();
            String user = gson.toJson(userTrasu);
            editor.putString(USER_TRASU, user);
        }
        editor.commit();
    }

    public void setUserTrasuTrasuNull() {
        editor.putString(USER_TRASU, null);
        editor.commit();
    }


    public ProblemaEntity getProblema() {
        String userData = preferences.getString(PROBLEMA, null);
        return new Gson().fromJson(userData, ProblemaEntity.class);
    }

    public void setProblema(ProblemaEntity problemaEntity) {
        if (problemaEntity != null) {
            Gson gson = new Gson();
            String user = gson.toJson(problemaEntity);
            editor.putString(PROBLEMA, user);
        }
        editor.commit();
    }

    public void setProblemaNull() {
        editor.putString(PROBLEMA, null);
        editor.commit();
    }

    public ServiceEntity getServiceTrasu() {
        String userData = preferences.getString(SERVICE_TRASU, null);
        return new Gson().fromJson(userData, ServiceEntity.class);
    }

    public void setServiceTrasu(ServiceEntity problemaEntity) {
        if (problemaEntity != null) {
            Gson gson = new Gson();
            String user = gson.toJson(problemaEntity);
            editor.putString(SERVICE_TRASU, user);
        }
        editor.commit();
    }

    public void setServiceTrasuNull() {
        editor.putString(SERVICE_TRASU, null);
        editor.commit();
    }

    public void setEnlaceComparatel(String enlace){
        editor.putString(ENLACE_COMPARATEL,enlace);
        editor.commit();
    }

    public void setEnlaceComparatelNull(){
        editor.putString(ENLACE_COMPARATEL,null);
        editor.commit();
    }

    public String getEnlaceComparatel() {
        return preferences.getString(ENLACE_COMPARATEL, null);
    }

    public void setEnlaceComparamovil(String enlace){
        editor.putString(ENLACE_COMPARAMOVIL,enlace);
        editor.commit();
    }

    public void setEnlaceComparamovilNull(){
        editor.putString(ENLACE_COMPARAMOVIL,null);
        editor.commit();
    }

    public String getEnlaceComparamovil() {
        return preferences.getString(ENLACE_COMPARAMOVIL, null);
    }

    public void setEnlacePunku(String enlace){
        editor.putString(ENLACE_PUNKU,enlace);
        editor.commit();
    }

    public void setEnlacePunkuNull(){
        editor.putString(ENLACE_PUNKU,null);
        editor.commit();
    }

    public String getEnlacePunku() {
        return preferences.getString(ENLACE_PUNKU, null);
    }


}
