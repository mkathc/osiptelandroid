package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class DistritoEntity implements Serializable {
    private int codigo;
    private String distrito;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }
}
