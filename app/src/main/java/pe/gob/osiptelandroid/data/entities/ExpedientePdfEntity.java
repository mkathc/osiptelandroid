package pe.gob.osiptelandroid.data.entities;

public class ExpedientePdfEntity  {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
