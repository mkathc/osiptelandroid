package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class ServiceEntity implements Serializable {
    private String idcodigo;
    private String descripcion;

    public String getIdCodigo() {
        return idcodigo;
    }

    public void setIdCodigo(String idCodigo) {
        this.idcodigo = idCodigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
