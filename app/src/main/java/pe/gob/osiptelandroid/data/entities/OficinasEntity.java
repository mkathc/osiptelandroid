package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class OficinasEntity implements Serializable {
    private String nombreOficina;
    private String direccion;
    private String longitud;
    private String latitud;
    private String horarioLV;
    private String horarioS;
    private String horarioD;
    private String telefono;

    public String getNombreOficina() {
        return nombreOficina;
    }

    public void setNombreOficina(String nombreOficina) {
        this.nombreOficina = nombreOficina;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getHorarioLV() {
        return horarioLV;
    }

    public void setHorarioLV(String horarioLV) {
        this.horarioLV = horarioLV;
    }

    public String getHorarioS() {
        return horarioS;
    }

    public void setHorarioS(String horarioS) {
        this.horarioS = horarioS;
    }

    public String getHorarioD() {
        return horarioD;
    }

    public void setHorarioD(String horarioD) {
        this.horarioD = horarioD;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
