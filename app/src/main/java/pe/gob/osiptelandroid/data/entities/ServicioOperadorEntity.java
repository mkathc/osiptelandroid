package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class ServicioOperadorEntity implements Serializable {
    private String idServicio;
    private String servicio;

    public ServicioOperadorEntity(String descripcion) {
        this.servicio = descripcion;
    }

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getDescripcion() {
        return servicio;
    }

    public void setDescripcion(String descripcion) {
        this.servicio = descripcion;
    }
}
