package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class VerReporteEntity implements Serializable {
    private String numEncuesta;
    private String nombre;
    private String empresa;
    private String recLlam;
    private String reaLlam;
    private String comentario;
    private String idCalidad;
    private String nivCob;
    private String marcaEquipo;
    private String so;
    private String modelo;
    private String correo;
    private String fechaVisita;
    private String fechaRegistro;
    private String lugar;

    public String getNumEncuesta() {
        return numEncuesta;
    }

    public void setNumEncuesta(String numEncuesta) {
        this.numEncuesta = numEncuesta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getRecLlam() {
        return recLlam;
    }

    public void setRecLlam(String recLlam) {
        this.recLlam = recLlam;
    }

    public String getReaLlam() {
        return reaLlam;
    }

    public void setReaLlam(String reaLlam) {
        this.reaLlam = reaLlam;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getIdCalidad() {
        return idCalidad;
    }

    public void setIdCalidad(String idCalidad) {
        this.idCalidad = idCalidad;
    }

    public String getNivCob() {
        return nivCob;
    }

    public void setNivCob(String nivCob) {
        this.nivCob = nivCob;
    }

    public String getMarcaEquipo() {
        return marcaEquipo;
    }

    public void setMarcaEquipo(String marcaEquipo) {
        this.marcaEquipo = marcaEquipo;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }
}
