package pe.gob.osiptelandroid.data.entities;

public class NroEncuesta {
    private String numeroEncuesta;

    public String getNumeroEncuesta() {
        return numeroEncuesta;
    }

    public void setNumeroEncuesta(String numeroEncuesta) {
        this.numeroEncuesta = numeroEncuesta;
    }
}
