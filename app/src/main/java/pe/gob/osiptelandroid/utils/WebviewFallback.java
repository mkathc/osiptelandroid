package pe.gob.osiptelandroid.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import pe.gob.osiptelandroid.presentation.verficarlinea.webview.WebViewActivity;

public class WebviewFallback implements CustomTabActivityHelper.CustomTabFallback {
    @Override
    public void openUri(Activity activity, Uri uri) {
        Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(WebViewActivity.EXTRA_URL, uri.toString());
        activity.startActivity(intent);
    }
}