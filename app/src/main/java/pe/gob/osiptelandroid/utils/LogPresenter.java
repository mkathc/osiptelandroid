package pe.gob.osiptelandroid.utils;

import android.content.Context;
import android.util.Log;

import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.PostRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogPresenter implements LogContract.Presenter {
    private Context context;
    private SessionManager mSessionManager;
    private String TAG = getClass().getName();

    public LogPresenter( Context context) {
        this.context = context;
        this.mSessionManager = new SessionManager(context);
    }

    @Override
    public void sendLog(BodyEntityLog log) {
        PostRequest postRequest = ServiceFactory.createService(PostRequest.class);
        Call<Void> orders = postRequest.sendLog("Bearer "+mSessionManager.getUserToken(),log);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG, response.toString());
                Log.d("Accion", log.getAccion());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }
}
