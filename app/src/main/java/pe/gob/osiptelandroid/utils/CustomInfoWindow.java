package pe.gob.osiptelandroid.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.data.entities.InfoWindowData;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomInfoWindow implements GoogleMap.InfoWindowAdapter {

    @BindView(R.id.tv_sede)
    TextView tvSede;
    @BindView(R.id.tv_direccion)
    TextView tvDireccion;
    @BindView(R.id.tv_horarios)
    TextView tvHorarios;
    private Context context;

    public CustomInfoWindow(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.info_window_oficinas, null);

        ButterKnife.bind(this, view);
        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        tvDireccion.setText(infoWindowData.getDireccion());
        tvSede.setText(infoWindowData.getSede());
        tvHorarios.setText(infoWindowData.getHorarios());

        return view;
    }
}