package pe.gob.osiptelandroid.utils;

import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;

public interface LogContract {
    interface Presenter {
        void sendLog(BodyEntityLog log);
    }
}
