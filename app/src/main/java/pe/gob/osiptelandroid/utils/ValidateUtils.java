package pe.gob.osiptelandroid.utils;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;

public class ValidateUtils {

    private Context mContext;
    private Activity mActivity;

    public ValidateUtils(Context context, Activity activity) {
        this.mContext = context;
        this.mActivity = activity;
    }

    public boolean validateEditTextString(EditText editText) {
        if (isEmpty(editText)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean validateEditTextNumber(EditText editText, int quantity) {
        if (isEmpty(editText)) {
            return false;
        } else {
            if (editText.length() != quantity) {
                return false;
            } else {
                return true;
            }
        }
    }

    private boolean isEmpty(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            editText.setBackground(mContext.getResources().getDrawable(R.drawable.border_error_edittext));
            return false;
        } else {
            return true;
        }
    }

    public void cleanUI(EditText editText, TextView textView, boolean isQuantity, int quantity) {

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.setBackground(mContext.getResources().getDrawable(R.drawable.border_edittext));
                if (textView != null) {
                    textView.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isQuantity) {
                    if (editText.length() == quantity) {
                        if (isOpenKeyboard()) {
                            hideKeyboard();
                        }
                    }
                }
            }
        });

        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    public void cleanUIRegister(EditText editText, TextView textView, boolean isQuantity, int quantity) {

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (textView != null) {
                    textView.setVisibility(View.GONE);
                }

                if (s.toString().equals(" ") && editText.getText().toString().length()==1) {
                    editText.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isQuantity) {
                    if (editText.length() == quantity) {
                        if (isOpenKeyboard()) {
                            hideKeyboard();
                        }
                    }
                }

            }
        });

        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(mContext.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
    }

    private boolean isOpenKeyboard() {
        InputMethodManager imm = (InputMethodManager) mActivity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) {
            return true;
        } else {
            return false;
        }
    }

}
